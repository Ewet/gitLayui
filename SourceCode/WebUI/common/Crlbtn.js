﻿
function showModel($,ArryModelId) {
    if ($('.app-model').is('.app-model-active')) {
        $('.app-model').removeClass('app-model-active');
    }
    $(ArryModelId).each(function (index, obj) {
        var Model = "#" + obj;
        $(Model).addClass('app-model-active');
    });
}

function ChooseBtn($, ArryBtnId) {

    $('.app-btn-group').removeClass('disabled');

    $(ArryBtnId).each(function (index, obj) {
        var btn = "#" + obj;
        $(btn).addClass('disabled');
    });
}