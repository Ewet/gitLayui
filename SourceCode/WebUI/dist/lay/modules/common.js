﻿/** onetree common.js-layui-v1.0.0.* */
/* Author:Javon Huang 2018/05/19 */
layui.define(['jquery', 'table', 'laydate', 'upload'], function (exports) {
    var form = layui.form
        , table = layui.table
        , laydate = layui.laydate
        , upload = layui.upload
        , $ = layui.jquery;
    var UploadExecl_baseUrl = "../WebUI/execljs/";
    Object.merge = function (e, r) { "object" != typeof e && (e = {}); var t = function (e, r) { if (r && "object" == typeof r) if (r instanceof Array) { e instanceof Array || (e = []); for (var n = 0; n < r.length; n++) e[n] = t(e[n], r[n]) } else e = Object.merge(e, r); else e = r; return e }; for (var n in r) r.hasOwnProperty(n) && (e[n] = t(e[n], r[n])); for (var o = 2, f = arguments.length; o < f; o++) Object.merge(e, arguments[o]); return e };
    var beforeSendfun = function (request) {
        var userinfo = JSON.parse(sessionStorage.getItem("userInfo"));
        if (userinfo !== null) {
            request.setRequestHeader("token", userinfo.token);
        }
    }
    var g_url = "http://139.159.226.56:8080";
    //全局监听发起ajax
    var userinfo = JSON.parse(sessionStorage.getItem("userInfo"));
    if (userinfo !== null) {
        $.ajaxSetup({
            headers: { // 默认添加请求头
                'token': userinfo.token
            }
        });
    }

    //全局监听ajaxerror
    $(document).ajaxError(function (event, XMLHttpRequest, options, exc) {
        if (XMLHttpRequest.status == 401) {
            var userinfo = sessionStorage.getItem("userInfo");
            if (userinfo !== null && typeof (userinfo) !== "undefined") {
                var Token = JSON.parse(sessionStorage.getItem("userInfo")).RefreshToken;
                obj.ajax_post("/api/LoginAPI/RefreshToken", { "RefreshToken": JSON.parse(sessionStorage.getItem("userInfo")).RefreshToken }, false,
                    function (data) {
                        if (data.Result === "success") {
                            window.sessionStorage.setItem("userInfo", JSON.stringify(data.data));
                            top.location.href = "portal.html";
                        } else {
                            layer.msg("登录过期，请重新登录", { icon: 5 });
                            setTimeout(function () {
                                top.location.href = "Login.html";
                            }, 2000);
                        }
                    });
            } else {
                layer.msg("登录过期，请重新登录", { icon: 5 });
                top.location.href = "Login.html";
            }
        } else if (XMLHttpRequest.status == 500) {
            layer.msg(JSON.parse(XMLHttpRequest.responseText).ExceptionMessage, { icon: 5 });
            setTimeout(function () {
                //top.location.href = "../login.html";  
            }, 2000);
        } else if (XMLHttpRequest.status == 405) {
            layer.msg("权限不足,请联系管理员", { icon: 5 });
        }
    });

    var obj = {
        GroupAuth: function (ModuleID, AuthId) {
            var blresult = false;
            blresult = $.each(userinfo.GroupModuleList, function (index, item) {
                if (item.ModuleID === ModuleID) {
                    $.each(item.GrpModuAuthList, function (_index, _item) {
                        if (_item.AuthId === AuthId) {
                            return true;
                        }
                    });
                }
            });
            return blresult;
        },
        ajax_g_url: function () {
            return g_url;
        },
        ajax_post: function (url, data, async, callback) {
            var a = typeof data
            $.ajax({
                url: g_url + url,
                async: async,
                type: "post",
                dataType: "json",
                contentType: typeof data == "string" ? 'application/json' : "application/x-www-form-urlencoded;charset=utf-8",
                data: data,
                success: callback,
                beforeSend: beforeSendfun,
            });
        },
        //ajax  get方式
        ajax_get: function (url, data, callback) {
            $.ajax({
                url: g_url + url,
                type: "get",
                dataType: "json",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data: data,
                beforeSend: beforeSendfun,
                success: callback,
            });
        },
        //下拉框数据绑定初始化
        Select: function (value, key, url, type, data, emid) {
            var emid = '#' + emid;
            $.ajax({
                url: g_url + url,
                type: type,
                dataType: "json",
                data: data,
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                beforeSend: beforeSendfun,
                success: function (res) {
                    var boards = eval(res.data);

                    $(boards).each(function (i, item) {
                        boards[i].id = boards[i][key.id];
                        boards[i].text = boards[i][key.text];
                    });

                    var optionStr = "<option value=''>请选择</option>";
                    $(boards).each(function (index) {
                        var board = boards[index];
                        optionStr += "<option value='" + board.id + "'>"
                            + board.text + "</option>";
                    });
                    $(emid).empty();
                    $(emid).prepend(optionStr);
                    form.render();
                    $(emid).next().find("dd[lay-value='" + value + "']").click();
                },
                error: function (data) {
                    layer.msg('数据接口错误');
                }
            });
        },
        SelectGroup: function (value, key, groupkey, url, type, data, emid) {
            var emid = '#' + emid;
            $.ajax({
                url: g_url + url,
                type: type,
                dataType: "json",
                data: data,
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                beforeSend: beforeSendfun,
                success: function (res) {
                    var boards = eval(res.data);
                    $(boards).each(function (i, item) {
                        boards[i].id = boards[i][key.id];
                        boards[i].text = boards[i][key.text];
                        $(boards[i][groupkey.child]).each(function (j, obj) {
                            boards[i][groupkey.child][j].id = boards[i][groupkey.child][j][groupkey.id];
                            boards[i][groupkey.child][j].text = boards[i][groupkey.child][j][groupkey.text];
                        });
                    });

                    var optionStr = "<option value=''>请选择</option>";
                    $(boards).each(function (index) {
                        //optionStr += "<optgroup value='" + boards[index].id + "'>";
                        optionStr += '<optgroup label="' + boards[index].text + '">';
                        $(boards[index][groupkey.child]).each(function (index, obj) {
                            optionStr += '<option value="' + obj.id + '">' + obj.text + '</option>';
                        });
                        optionStr += "</optgroup>";
                    });
                    $(emid).empty();
                    $(emid).prepend(optionStr);
                    form.render();
                    $(emid).next().find("dd[lay-value='" + value + "']").click();
                },
                error: function (data) {
                    layer.msg('数据接口错误');
                }
            });
        },

        //设置下拉框默认选中值
        Select_render: function (arry_value, emid) {
            var emid = '#' + emid;
            $(arry_value).each(function (i, item) {
                $(emid).next().find("dd[lay-value='" + item + "']").click();
            });
        },
        //select2控件附带检索框
        Select2: function (arry_value, key, url, type, data, emid) {
            var inputId = emid;
            var emid = '#' + emid;
            $.ajax({
                url: g_url + url,
                type: type,
                dataType: "json",
                data: data,
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                success: function (data) {
                    var boards = eval(data.data);
                    var optionStr = "<option value=''>请选择</option>";
                    $(boards).each(function (i, item) {
                        boards[i].id = boards[i][key.id];
                        boards[i].text = boards[i][key.text];
                    });
                    $(boards).each(
                        function (index) {
                            var board = boards[index];

                            optionStr += "<option value='" + board.id + "'>"
                                + board.text + "</option>";
                        });
                    $(emid).empty();
                    $(emid).prepend(optionStr);
                    // form.render();
                    Init($, emid);
                    $(arry_value).each(function (i, item) {
                        $(emid).next().find("dd[lay-value='" + item + "']").click();
                    });
                    $(emid).next().find("dl").prepend("<dd><input class='txtSeach2" + inputId + "' style='height:30px;width:100%;'/></dd>");
                    var classname = '.txtSeach2' + inputId;
                    $(classname).bind('input propertychange', function () {
                        var b = this.value;
                        var ddid = $(this).parents().nextAll("dd");
                        $(ddid).each(function (index, item) {
                            var c = item.innerText
                            if (c.indexOf(b) >= 0) {
                                item.style.display = "";
                            } else {
                                item.style.display = "none";
                            }
                        });
                    });
                },
                error: function (data) {
                    layer.msg('数据接口错误');
                }
            });
            function Init(a, id) {
                a(id).each(function (k, e) {
                    var f = "", d = "", c = ""; a(this).find("option").each(function (h, b) {
                        if (0 != h || null != a(this).attr("val") && "" !== a(this).attr("val")) {
                            h = a(this).attr("value"); b = a(this).text(); var g = a(this).attr("img"), e = a(this).is(":selected"); f += '<dd lay-value="' + h + '" class="' + (e ? "layui-this" : "") + '">'; f += '<i style="display:inline-block;width:16px;height:16px;border:1px solid #e6e6e6;vertical-align:middle;margin-right:5px;line-height:19px;text-align:center;">' +
                                (e ? "&radic;" : "") + "</i>"; null != g && "" != g && (g = eval("(" + g + ")"), null != g.src && "" != g.src && (f += '<img2 src="' + g.src + '" height="16" width="16" style="margin-right:5px;vertical-align:middle;' + g.css + '"/>')); f += b; f += "</dd>"; e && (d += "," + h, c += "," + b)
                        }
                    }); "" != d && (d = d.substring(1)); "" != c && (c = c.substring(1)); a(this).siblings("div.layui-form-select").find("dl").html(f); a(this).before('<input type="hidden" class="txtSel" name="' + a(this).attr("name") + '" value="' + d + '">'); a(this).removeAttr("name"); a(this).siblings("div.layui-form-select").find(".layui-select-title input").val(c).attr("lay-verify",
                        a(this).attr("lay-verify")); a(this).siblings("div.layui-form-select").find("dd").each(function (e, b) {
                            a(this).click(function () {
                                c = d = ""; a(this).hasClass("layui-this") ? a(this).removeClass("layui-this").find("i").text("") : a(this).addClass("layui-this").find("i").html("&radic;"); a(this).parent().find("dd.layui-this").each(function () { d += "," + a(this).attr("lay-value"); c += "," + a(this).text().substring(1) }); "" != d && (d = d.substring(1)); "" != c && (c = c.substring(1)); a(this).parent().parent().siblings("input.txtSel").val(d);
                                a(this).parent().siblings(".layui-select-title").find("input.layui-input").val(c); var b = {}; b.elem = a(this).parent().parent().siblings("select"); b.value = a(this).attr("lay-value");
                            })
                        })
                })
            }
        },
        //select2检索功能
        Select2_Seach_render: function () {
            $(".select_seach_render").click(function (e) { // 在页面任意位置点击select_seach_render样式而触发此事件
                var emid = $(this).children("select").attr("id");
                var classname = '.txtSeach2' + emid;
                $(classname).val("");
                $(classname).focus(); // e.target表示被点击的目标
                var ddid = $(classname).parents().nextAll();
                $(ddid).each(function (index, item) {
                    item.style.display = "block";
                });
            })
        },
        //整形下拉数据
        formSelects2: function (value, key, data) {
            var result = { arr: "" };
            var list = new Array();
            $(data).each(function (i, item) {
                var boards = {
                    name: "",
                    value: "",
                    selected: "",
                    disabled:""
                }
                if (data[i][key.id] === value) {
                    boards.value = data[i][key.id];
                    boards.name = data[i][key.text];
                    boards.selected = "selected";
                    boards.disabled = "";
                } else {
                    boards.value = data[i][key.id];
                    boards.name = data[i][key.text];
                    boards.selected = "";
                    boards.disabled = "";
                }
                list.push(boards);
            });
            result.arr = list;
            return result;
        },
        //table内日期控件,数据条数,日历输入框的id前缀,tableId,日期时间绑定的列,默认的时间
        table_date_render: function (count, inputId, tableId, fileName) {
            for (var i = 0; i < count; i++) {
                var strid = '#' + inputId + i;
                laydate.render({
                    elem: strid
                    //, format: 'yyyy/MM/dd'
                    //, value: defaultData
                    , index: i
                    , done: function (value, date, endDate) {
                        if (value != "") {
                            var a = "#" + this.elem[0].id;
                            var index = this.index;
                            var tempValue = table.cache[tableId];
                            tempValue[index][fileName] = value;
                            table.cache[tableId] = tempValue;
                            $(a).val(value);
                        }
                    }
                });
            }
        },
        //table内select控件
        table_select_render: function (h, e, c) {
            var tk;
            var obj;
            var otd;
            var callback;
            callback = c;
            var d = document.getElementById('table_type_select');
            if (d) {
                var _parentElement = d.parentNode;
                if (_parentElement) {
                    _parentElement.removeChild(d);
                }
            }
            otd = e;
            put = e;
            var s = '<div id="table_type_select" class="table-select"><dl class="layui-anim layui-anim-upbit" style="padding: 0px;top: 0px"><input class="demo" id="txtSeach" style="height:30px;width:100%;"/><div id="sel">';
            $(h.data).each(function (i, item) {
                h.data[i].value = h.data[i][h.value_key];
                h.data[i].text = h.data[i][h.text_key];
            });
            h.data.splice(0, 0, { value: "", text: "请选择" });
            for (var k in h.data) {
                if (h.value == h.data[k].value) {
                    s += '<dd lay-value="' + h.data[k].value + '" class="layui-this">' + h.data[k].text + '</dd>';
                } else {
                    s += '<dd lay-value="' + h.data[k].value + '" >' + h.data[k].text + '</dd>';
                }
            }
            s += '<div></dl></div>';
            otd.innerHTML = s + otd.innerHTML;
            obj = document.getElementById('table_type_select');
            obj.onmouseout = function () {
                tk = 1;
                setTimeout(function () {
                    if (tk) {
                        if (obj) {
                            ke = 0;
                            var _parentElement = obj.parentNode;
                            if (_parentElement) {
                                _parentElement.removeChild(obj);
                            }
                        }
                    }
                }, 200);
            }
            obj.onmouseover = function () {
                tk = 0;
            }
            //IE9以下不兼容
            $("#txtSeach").focus();

            $('#txtSeach').bind('input propertychange', function () {
                var b = this.value;
                $("#sel dd").each(function (index, item) {
                    var c = item.innerHTML;
                    if (c.indexOf(b) >= 0) {
                        item.style.display = "";
                    } else {
                        item.style.display = "none";
                    }
                });
            });

            obj2 = document.getElementById('sel');
            obj2.addEventListener('click', function (e) {
                //e.srcElement; 火狐不支持srcElement                  
                var value = $(e.target).attr('lay-value');
                var text = e.target.innerHTML;

                var _parentElement = obj2.parentNode.parentNode;
                if (_parentElement) {
                    _parentElement.removeChild(obj2.parentNode);
                }
                callback({ value: value, text: text });
            });
        },
        //table行内select控件初始化
        Init_table_select_render: function (defaultValue, url, where, value, descr, e, callback) {
            obj.ajax_post(url, where, true,
                function (data) {
                    if (data.Result === "success") {
                        var bind_data = {
                            value: defaultValue,
                            data: data.data,
                            value_key: value,
                            text_key: descr
                        };
                        obj.table_select_render(bind_data, e, callback);
                    } else {
                        layer.msg('数据接口错误');
                    }
                });
        },
        //table input栏位回车跳转
        table_input_render: function () {
            $(document).on('keydown', '.layui-input', function (e) {
                if (e.keyCode === 13) {
                    var $el = $(e.target)
                    var $td = $el.parents('td')
                    var $thisTr = $td.parents('tr')
                    var $nextTd = $td.next()

                    if ($nextTd.length) {
                        var a = $thisTr.find('td').eq($nextTd.index()).trigger('click');
                        var b = a.find('input');
                        b.select();
                        if (!b.length) {
                            var $td = $el.parents('td')
                            var $nextTr = $td.parents('tr').next()
                            var item = $td.index();
                            if ($nextTr.length) {
                                for (i = 1; i < item; i++) {
                                    var thisClick = $nextTr.find('td').eq(i).trigger('click');
                                    if (thisClick.find('input').length) {
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            })
        },
        //设置行颜色
        table_Set_columnColor: function (TableIns, res, callback) {
            var str = "div[lay-filter='LAY-table-" + TableIns.config.index + "']";

            $(res.data).each(function (i, data_item) {
                var objResult = callback(data_item);
                if (objResult.blresult) {
                    var obj = $(str).children(".layui-table-box").children(".layui-table-body ").find('table tbody').find("tr[data-index='" + res.data[i].LAY_TABLE_INDEX + "']");
                    $(obj).each(function (index, item) {
                        $(this).css("background-color", objResult.color);
                    });
                }
            });
        },
        //table对象,比对值，比对字段，默认勾选
        table_Check_Init: function (TableIns, defualtvalue, field) {
            var g_index = new Array();
            var str = "div[lay-filter='LAY-table-" + TableIns.config.index + "']";
            var obj = $(str).children(".layui-table-box").children(".layui-table-body ").find('table tbody').find("td[data-field='" + field + "']");

            $(obj).each(function (index, item) {
                if (defualtvalue === item.innerText) {
                    var Num = item.parentNode.dataset.index;
                    g_index.push(Num);
                }
            });
            //$(str + ' tr[data-index="' + g_index+'"] .layui-form-checkbox').trigger("click");
            $(g_index).each(function (index, item) {
                $(str + ' tr[data-index="' + item + '"] .layui-form-checkbox').each(function (index, obj) {
                    $(this).trigger("click");
                });
            });
        },
        //检查table各个列值类型,检查类型函数,表ID,检查的字段(可多字段检查),检查整表/勾选行,错误提示,
        table_Check_columnVaule: function (checkfunction, tableId, o_field, blCheck, message) {
            var tempValue = null;
            var Result = false;
            if (blCheck) {
                tempValue = table.checkStatus(tableId).data;
            } else {
                tempValue = table.cache[tableId];
            }

            $(tempValue).each(function (i, item) {
                $(o_field).each(function (j, field) {
                    var a = tempValue[i][field];
                    Result = checkfunction(a);
                    if (!Result) {
                        layer.msg(message, { icon: 5 });
                        return false;
                    }
                });
            });
            return Result;
        },
        //table监控Switch状态-动态更新表字段值时判断用到-表头联动不可用
        table_Client_Switch: function (obj) {
            return $(obj.childNodes['0'].childNodes['2']).hasClass('layui-form-checked');
        },
        //table监控表头Switch与列联动
        table_Client_SwitchJoinHeader: function (clounmfiltername, headercheck) {
            var arr = new Array();
            $($("input[lay-filter='" + clounmfiltername + "']")).each(function (index, obj) {
                if (!this.checked === headercheck) {
                    $(this).trigger("click");
                    arr.push(this.value);
                }
            });
            form.render();
            return arr;
        },
        //table监控列Switch与表头联动
        table_Client_SwitchJoinColunm: function (clounmfiltername, headerfiltername) {
            var result = false;
            $($("input[lay-filter='" + clounmfiltername + "']")).each(function (index, obj) {
                if (this.checked) {
                    result = this.checked;
                } else {
                    result = this.checked;
                    return false;
                }
            });

            $($("input[lay-filter='" + headerfiltername + "']")).each(function (index, obj) {
                if (!this.checked === result) {
                    $(this).trigger("click");
                }
            });

            form.render();
        },
        //动态添加行
        table_add_column: function (tableId, data, initTable) {
            var tempValue = table.cache[tableId];
            var index = tempValue.length + 1;
            data.LAY_TABLE_INDEX = index;
            tempValue.push(data);
            initTable(tempValue);
        },
        //删除动态添加的行
        table_del_column: function (tableId) {
            var tempValue = table.cache[tableId];
            $(tempValue).each(function (i, item) {
                if (item.length === 0) {
                    tempValue.splice(i, 1);
                }
            });
            table.cache[tableId] = tempValue;
        },
        //删除table数据的列数据
        table_del_file: function (tableId, arry_file) {
            var tempValue = table.cache[tableId];
            if (tempValue.length === 0) { return null; }
            var resultValue = Object.merge({}, tempValue);
            $(resultValue).each(function (i, item) {
                delete (resultValue[i].LAY_TABLE_INDEX);
                $(arry_file).each(function (j, i_file) {
                    delete (resultValue[i][i_file]);
                });
            });
            return resultValue;
        },
        //编辑时数据回滚
        table_edit_rollback: function (tableId, tableObj, fileValue, obj) {
            var str = "div[lay-filter='LAY-table-" + tableObj.config.index + "']";
            $(str).children(".layui-table-box").children(".layui-table-body ").find('table tbody').find("tr[data-index='" + obj.data.LAY_TABLE_INDEX + "']").find("td[data-field='" + obj.field + "']").find("input")[0].value = fileValue;
            var tempValue = table.cache[tableId];
            for (var key in tempValue[obj.data.LAY_TABLE_INDEX]) {
                if (key === obj.field) { tempValue[obj.data.LAY_TABLE_INDEX][key] = fileValue; }
            }
            table.cache[tableId] = tempValue;
        },
        //table编辑栏固定在右边检索功能
        table_search_rtool_render: function (input_name, tableObj) {
            var obj_input = "input[name='" + input_name + "']";
            $(obj_input).unbind();
            $(obj_input).bind('input propertychange', function () {
                var b = this.value;
                var str = "div[lay-filter='LAY-table-" + tableObj.config.index + "']";
                var table_tr = $(str).children(".layui-table-box").children(".layui-table-body").find('table tbody').find("tr");

                $(table_tr).each(function (index, item) {
                    var c = item.innerText
                    if (c.indexOf(b) >= 0) {
                        item.style.display = "";
                        $(str).children(".layui-table-box").children(".layui-table-fixed-r").children(".layui-table-body").find('table tbody').find('tr[data-index="' + item.rowIndex + '"]')[0].style.display = "";
                    } else {
                        item.style.display = "none";
                        $(str).children(".layui-table-box").children(".layui-table-fixed-r").children(".layui-table-body").find('table tbody').find('tr[data-index="' + item.rowIndex + '"]')[0].style.display = "none";
                    }
                });
            });
        },
        //table编辑栏固定在左边检索功能
        table_search_ltool_render: function (input_name, tableObj) {
            var obj_input = "input[name='" + input_name + "']";
            $(obj_input).unbind();
            $(obj_input).bind('input propertychange', function () {
                var b = this.value;
                var str = "div[lay-filter='LAY-table-" + tableObj.config.index + "']";
                var table_tr = $(str).children(".layui-table-box").children(".layui-table-body").find('table tbody').find("tr");

                $(table_tr).each(function (index, item) {
                    var c = item.innerText
                    if (c.indexOf(b) >= 0) {
                        item.style.display = "";
                        $(str).children(".layui-table-box").children(".layui-table-fixed-l").children(".layui-table-body").find('table tbody').find('tr[data-index="' + item.rowIndex + '"]')[0].style.display = "";
                    } else {
                        item.style.display = "none";
                        $(str).children(".layui-table-box").children(".layui-table-fixed-l").children(".layui-table-body").find('table tbody').find('tr[data-index="' + item.rowIndex + '"]')[0].style.display = "none";
                    }
                });
            });
        },
        //table编辑栏固定在左右两边检索功能
        table_search_lrtool_render: function (input_name, tableObj) {
            var obj_input = "input[name='" + input_name + "']";
            $(obj_input).unbind();
            $(obj_input).bind('input propertychange', function () {
                var b = this.value;
                var str = "div[lay-filter='LAY-table-" + tableObj.config.index + "']";
                var table_tr = $(str).children(".layui-table-box").children(".layui-table-body").find('table tbody').find("tr");

                $(table_tr).each(function (index, item) {
                    var c = item.innerText
                    if (c.indexOf(b) >= 0) {
                        item.style.display = "";
                        $(str).children(".layui-table-box").children(".layui-table-fixed-l").children(".layui-table-body").find('table tbody').find('tr[data-index="' + item.rowIndex + '"]')[0].style.display = "";
                        $(str).children(".layui-table-box").children(".layui-table-fixed-r").children(".layui-table-body").find('table tbody').find('tr[data-index="' + item.rowIndex + '"]')[0].style.display = "";
                    } else {
                        item.style.display = "none";
                        $(str).children(".layui-table-box").children(".layui-table-fixed-l").children(".layui-table-body").find('table tbody').find('tr[data-index="' + item.rowIndex + '"]')[0].style.display = "none";
                        $(str).children(".layui-table-box").children(".layui-table-fixed-r").children(".layui-table-body").find('table tbody').find('tr[data-index="' + item.rowIndex + '"]')[0].style.display = "none";
                    }
                });
            });
        },
        //table编辑栏不固定检索功能
        table_search_render: function (input_name, tableObj) {
            var obj_input = "input[name='" + input_name + "']";
            $(obj_input).unbind();
            $(obj_input).bind('input propertychange', function () {
                var b = this.value;
                var str = "div[lay-filter='LAY-table-" + tableObj.config.index + "']";
                var table_tr = $(str).children(".layui-table-box").children(".layui-table-body").find('table tbody').find("tr");

                $(table_tr).each(function (index, item) {
                    var c = item.innerText
                    if (c.indexOf(b) >= 0) {
                        item.style.display = "";
                    } else {
                        item.style.display = "none";
                    }
                });
            });
        },
        //绑定表头工具栏事件
        table_toolbar_event: function (id, func) {
            var docid = '#' + id;
            var obj_input = $(docid);
            $(obj_input).unbind();
            $(obj_input).bind('click', func);
        },
        //缓存数据
        Save_localStorage: function (key_name, obj) {
            var templete = JSON.parse(sessionStorage.getItem(key_name));
            if (templete === null) {
                templete = [];
            }
            $(obj).each(function (i, item) {
                templete.push(item);
            });
            sessionStorage.setItem(key_name, JSON.stringify(templete));
        },
        //删除缓存数据元素
        Delete_Save_localStorage_Element: function (key_name, value) {
            var templete = JSON.parse(sessionStorage.getItem(key_name));
            if (templete === null) {
                templete = [];
            }
            $(templete).each(function (i, item) {
                if (templete[i] === value) {
                    templete.splice(i - 1, 1);
                }
            });
            sessionStorage.setItem(key_name, JSON.stringify(templete));
        },
        //图片上传及初始化
        img_upload_render: function (d_param, u_param, imgsize, skin) {
            window.sessionStorage.setItem("Imgskin", skin);
            var demoListView = $('#' + u_param.listname + '');
            $.ajax({
                url: g_url + d_param.geturl,
                type: "post",
                dataType: "json",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data: d_param.requstparam,
                success: function (data) {
                    var data = data.data;
                    $(data).each(function (i, item) {
                        var strinit = $(['<div id="upload-' + data[i][d_param.value] + '" class="upload-img2" name="' + data[i][d_param.value] + '">'
                            + '<img2 src="' + d_param.path + data[i][d_param.value] + '" class="layui-upload-img2 upload-img2-list">'
                            + '<div class="upload-img2-btn-div">'
                            + '<button class="layui-btn layui-btn-xs layui-btn-danger init-delete">删除<i class="layui-icon">&#xe640;</i></button><span class="layui-badge layui-btn-xs layui-bg-green upload-img2-span">已存在</span>'
                            + '</div></div>'].join(''));

                        demoListView.append(strinit);
                        InitCss(imgsize.height, imgsize.width);
                        switch (window.sessionStorage.getItem("Imgskin")) {
                            case "0":
                                break;
                            case "1":
                                $(".upload-img2-list").height("100%");
                                $(".upload-img2-btn-div").css("bottom", 0);
                                $(".upload-img2-btn-div").css("position", "absolute");
                                $(".upload-img2-btn-div").css("display", "none");
                                $(".upload-img2").hover(function () {
                                    $(this).find("div").css("display", "block");
                                }, function () {
                                    $(this).find("div").css("display", "none");
                                });
                                break;
                        }
                        strinit.find('.init-delete').on('click', function (e) {
                            var imgid = $(e.target).parent().parent().attr("name");

                            var layer_index = layer.confirm('确定删除么？', {
                                btn: ['确定', '取消'], //按钮
                                icon: 2
                            }, function () {
                                deleteImg(imgid, d_param.delurl, strinit);
                            }, function () {
                                layer.close(layer_index);
                            });
                        });
                    });
                },
                beforeSend: function (request) {
                    //request.setRequestHeader("token", token);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    layer.msg('图片获取接口错误', { icon: 2 });
                }
            });

            var uploadListIns = upload.render({
                elem: '#' + u_param.elem
                , url: g_url + u_param.url
                , data: u_param.requstparam
                , size: u_param.size //限制文件大小，单位 KB
                , accept: u_param.accept
                , multiple: true
                , auto: false
                , bindAction: '#' + u_param.bindaction
                , choose: function (obj) {
                    var files = this.files = obj.pushFile(); //将每次选择的文件追加到文件队列
                    //读取本地文件
                    obj.preview(function (index, file, result) {

                        var strImg = $(['<div id="upload-' + index + '" class="upload-img2">'
                            + '<img2 src="' + result + '" alt="' + file.name + '" class="layui-upload-img2 upload-img2-list">'
                            + '<div class="upload-img2-btn-div">'
                            + '<button class="layui-btn layui-btn-xs layui-btn-danger demo-delete">删除<i class="layui-icon">&#xe640;</i>'
                            + '</button><button class="layui-btn layui-btn-xs layui-btn-mini demo-reload layui-hide">重传</button>'
                            + '<span class="layui-badge layui-btn-xs layui-bg-green upload-img2-span">' + (file.size / 1014).toFixed(1) + 'kb</span>'
                            + '</div></div>'].join(''));

                        //单个重传
                        strImg.find('.demo-reload').on('click', function () {
                            obj.upload(index, file);
                        });

                        //删除
                        strImg.find('.demo-delete').on('click', function () {

                            var strImg_index = layer.confirm('确定删除么？', {
                                btn: ['确定', '取消'], //按钮
                                icon: 2
                            }, function () {
                                delete files[index]; //删除对应的文件
                                strImg.remove();
                                uploadListIns.config.elem.next()[0].value = ''; //清空 input file 值，以免删除后出现同名文件不可选
                                layer.close(strImg_index);
                            }, function () {
                                layer.close(strImg_index);
                            });
                        });
                        demoListView.append(strImg);
                        InitCss(imgsize.height, imgsize.width);
                        switch (window.sessionStorage.getItem("Imgskin")) {
                            case "0":
                                break;
                            case "1":
                                $(".upload-img2-list").height("100%");
                                $(".upload-img2-btn-div").css("bottom", 0);
                                $(".upload-img2-btn-div").css("position", "absolute");
                                $(".upload-img2-btn-div").css("display", "none");
                                $(".upload-img2").hover(function () {
                                    $(this).find("div").css("display", "block");
                                }, function () {
                                    $(this).find("div").css("display", "none");
                                });
                                break;
                        }
                    });
                }
                , done: function (res, index, upload) {
                    if (res.data.length > 0) { //上传成功
                        var strdome = demoListView.find('div#upload-' + index), strdomechirld = strdome.children();
                        strdomechirld.eq(1).find('.upload-img2-span').html('上传成功');
                        strdomechirld.eq(1).find('.demo-delete').remove();
                        //strdome.remove(); //隐藏重传
                        return delete this.files[index]; //删除文件队列已经上传成功的文件
                    }
                    this.error(index, upload);
                }
                , error: function (index, upload) {

                    var strdome = demoListView.find('div#upload-' + index), strdomechirld = strdome.children();
                    strdomechirld.eq(1).find('.demo-reload').removeClass('layui-hide'); //显示重传
                }
            });

            function deleteImg(imgid, url, htmlObj) {
                $.ajax({
                    url: g_url + url,
                    type: "post",
                    dataType: "json",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data: { imgId: imgid },
                    success: function (data) {
                        layer.msg('图片删除成功', { icon: 1 });
                        htmlObj.remove();
                    },
                    beforeSend: function (request) {
                        //request.setRequestHeader("token", token);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        layer.msg('图片删除失败', { icon: 2 });
                    }
                });
            }

            function InitCss(intheight, intwidth) {
                $(".upload-img2").css("display", "inline-block");
                $(".upload-img2").css("position", "relative");
                $(".upload-img2").css("border", "solid 1px #a8a0a0");
                $(".upload-img2").css("margin", "5px");

                $(".upload-img2-list").width("100%");
                $(".upload-img2-list").css("border-bottom", "solid 1px #a09b9b");

                $(".upload-img2-btn-div").css("padding", "5px 5px");

                $(".upload-img2-span").height("22px");
                $(".upload-img2-span").css("line-height", "22px");
                $(".upload-img2-span").css("display", "inline-block");
                $(".upload-img2-span").css("margin-left", "5px");
                $(".upload-img2-span").css("margin-top", "0px");

                $(".upload-img2").height(intheight < 50 ? 50 : imgsize.height);
                $(".upload-img2").width(intwidth < 165 ? 165 : intwidth);
                $(".upload-img2-list").height((intheight - 30) < 20 ? 20 : (intheight - 30));
            }
        },
        //Execl转JSON格式
        execl_upload_render: function (filebtnId, upUrl, upbtnId, storageName, sheetName, setuploadobj) {
            upload.render({
                elem: '#' + filebtnId
                , url: ''
                , auto: false
                , accept: 'file' //普通文件
                , exts: 'xlsx|xls' //只允许上传压缩文件
                , choose: function (obj) {
                    //预读本地文件示例，不支持ie8
                    obj.preview(function (index, file, result) {
                        $('#' + upbtnId + '').addClass('layui-btn-disabled');
                        $('#' + filebtnId + '').parent().find('span').remove();
                        $('#' + filebtnId + '').after('<span class="layui-inline layui-upload-choose">' + file.name + '</span>');
                        ChangeExeclToJSON(file);
                    });
                }
            });
            function ChangeExeclToJSON(fileSourse) {
                $.getScript(UploadExecl_baseUrl + "shim.js");
                $.getScript(UploadExecl_baseUrl + "xlsx.full.min.js", function () {  //加载test.js,成功后，并执行回调函数
                    console.log("加载js文件");

                    var X = XLSX;
                    var XW = {
                        msg: "xlsx",
                        worker: UploadExecl_baseUrl + "xlsxworker.js"
                    };
                    var global_wb;

                    var process_wb = (function () {

                        var to_json = function to_json(workbook) {
                            var result = {};
                            workbook.SheetNames.forEach(function (sheetName) {
                                var roa = X.utils.sheet_to_json(workbook.Sheets[sheetName], { header: 1 });
                                if (roa.length) result[sheetName] = roa;
                            });
                            return JSON.stringify(result, 2, 2);
                        };


                        return function process_wb(wb) {
                            global_wb = wb;
                            var output = "";
                            output = to_json(wb);
                            var outDateSourse = JSON.parse(output)[sheetName];
                            outDateSourse.splice(0, 1);

                            var resultdata = new Array();
                            $(outDateSourse).each(function (index, obj) {
                                var myObj = setuploadobj(obj);
                                resultdata.push(myObj);
                            });

                            window.sessionStorage.setItem(storageName, JSON.stringify(resultdata));
                            $('#' + upbtnId + '').removeClass('layui-btn-disabled');
                        };
                    })();

                    var setfmt = window.setfmt = function setfmt() { if (global_wb) process_wb(global_wb); };

                    var b64it = window.b64it = (function () {
                        var tarea = document.getElementById('b64data');
                        return function b64it() {
                            if (typeof console !== 'undefined') console.log("onload", new Date());
                            var wb = X.read(tarea.value, { type: 'base64', WTF: false });
                            process_wb(wb);
                        };
                    })();

                    var do_file = (function () {
                        var rABS = typeof FileReader !== "undefined" && (FileReader.prototype || {}).readAsBinaryString;

                        var xw = function xw(data, cb) {
                            var worker = new Worker(XW.worker);
                            worker.onmessage = function (e) {
                                switch (e.data.t) {
                                    case 'ready': break;
                                    case 'e': console.error(e.data.d); break;
                                    case XW.msg: cb(JSON.parse(e.data.d)); break;
                                }
                            };
                            worker.postMessage({ d: data, b: rABS ? 'binary' : 'array' });
                        };

                        return function do_file(files) {
                            var f = files;
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var data = e.target.result;
                                if (!rABS) data = new Uint8Array(data);
                                xw(data, process_wb);
                            };
                            if (rABS) reader.readAsBinaryString(f);
                            else reader.readAsArrayBuffer(f);
                        };
                    })();

                    do_file(fileSourse);
                });
            }

            $('#' + upbtnId + '').click(function () {
                var a = JSON.parse(sessionStorage.getItem(storageName));
                if (a != null) {
                    $.ajax({
                        url: g_url + upUrl,
                        type: 'post',
                        //dataType: "json",
                        data: JSON.stringify(a),
                        contentType: "application/json",
                        success: function (res) {
                            layer.msg('上传成功', { icon: 1 });
                            window.sessionStorage.setItem(storageName, null);
                            $('#' + upbtnId + '').addClass('layui-btn-disabled');
                        },
                        error: function (data) {
                            layer.msg('数据接口错误', { icon: 5 });
                        }
                    });
                } else {
                    $('#' + upbtnId + '').addClass('layui-btn-disabled');
                }
            });
        },

        //Execl导出
        execl_export_render: function (tableId, filename, bookType, rename, delkey) {
            var tempValue = tableId;
            if (typeof (tempValue) == "undefined" || tempValue.length === 0) {
                layer.msg("没有数据", { icon: 5 });
                return false;
            }
            $(tempValue).each(function (i, item) {
                delete (tempValue[i].LAY_TABLE_INDEX);
                $(delkey).each(function (j, i_file) {
                    delete (tempValue[i][i_file]);
                });
            });

            var strjson = JSON.stringify(tempValue);

            $(rename).each(function (i, item) {
                var re = new RegExp(item[0], "g");
                strjson = strjson.replace(re, item[1]);
            });
            tempValue = JSON.parse(strjson);
            var wopts = "";
            switch (bookType) {
                case 'xlsx':
                    wopts = { bookType: 'xlsx', bookSST: false, type: 'binary' };
                    break;
                case 'csv':
                    wopts = { bookType: 'csv', bookSST: false, type: 'binary' };
                    break;
                case 'ods':
                    wopts = { bookType: 'ods', bookSST: false, type: 'binary' };
                    break;
                case 'xlsb':
                    wopts = { bookType: 'xlsb', bookSST: false, type: 'binary' };
                    break;
                case 'fods':
                    wopts = { bookType: 'fods', bookSST: false, type: 'binary' };
                    break;
                case 'biff2':
                    wopts = { bookType: 'biff2', bookSST: false, type: 'binary' };
                    break;
            }
            downloadExl(tempValue);
            function downloadExl(data) {
                var wb = { SheetNames: ['Sheet1'], Sheets: {}, Props: {} };
                wb.Sheets['Sheet1'] = XLSX.utils.json_to_sheet(data);//通过json_to_sheet转成单页(Sheet)数据
                saveAs(new Blob([s2ab(XLSX.write(wb, wopts))], { type: "application/octet-stream" }), filename + '.' + (wopts.bookType == "biff2" ? "xls" : wopts.bookType));
            }
            function s2ab(s) {
                if (typeof ArrayBuffer !== 'undefined') {
                    var buf = new ArrayBuffer(s.length);
                    var view = new Uint8Array(buf);
                    for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
                    return buf;
                } else {
                    var buf = new Array(s.length);
                    for (var i = 0; i != s.length; ++i) buf[i] = s.charCodeAt(i) & 0xFF;
                    return buf;
                }
            }
        },

        replace: function (data, rename) {
            var strjson = JSON.stringify(data);
            $(rename).each(function (i, item) {
                var re = new RegExp(item[0], "g");
                strjson = strjson.replace(re, item[1]);
            });
            data = JSON.parse(strjson);
            return data;
        },
        //获取当前系统时间
        nowDate: function (data) {
            if (typeof data == 'undefined') { data = null }
            var year = 0;
            var mon = 0;
            var day = 0;
            var h = 0;
            var m = 0;
            var s = 0;
            for (var key in data) {
                switch (key) {
                    case "year":
                        year = data[key];
                        break;
                    case "mon":
                        mon = data[key];
                        break;
                    case "day":
                        day = data[key];
                        break;
                    case "h":
                        h = data[key];
                        break;
                    case "m":
                        m = data[key];
                        break;
                    case "s":
                        s = data[key];
                        break;
                }
            }
            var dateObj = new Object();
            var myDate = new Date();
            myDate.setFullYear(myDate.getFullYear() + year);
            myDate.setMonth(myDate.getMonth() + mon);
            myDate.setDate(myDate.getDate() + day);
            myDate.setHours(myDate.getHours() + h);
            myDate.setMinutes(myDate.getMinutes() + m);
            myDate.setSeconds(myDate.getSeconds() + s);

            var shortDate = (myDate.getFullYear()) + "-" + ((myDate.getMonth() + 1) < 10 ? "0" + (myDate.getMonth() + 1) : myDate.getMonth() + 1) + "-" + myDate.getDate();
            var longDate = (myDate.getFullYear()) + "-" + ((myDate.getMonth() + 1) < 10 ? "0" + (myDate.getMonth() + 1) : myDate.getMonth() + 1) + "-" + myDate.getDate() + " " + myDate.getHours() + ":" + myDate.getMinutes() + ":" + myDate.getSeconds();
            dateObj.shortDate = shortDate;
            dateObj.longDate = longDate;
            return dateObj;
        },
        //验证是否是为空 
        input_IsNull: function (value) {
            if (value === "" || value === null || typeof (value) == "undefined") {
                return false;
            } else {
                return true;
            }
        },
        //验证是否是数字 
        input_IsNum: function (value) {
            if (isNaN(value)) {
                return false;
            } else {
                return true;
            }
        },
        //验证是否是数字0和正数
        input_IsInit: function (value) {
            var reg = /^([1-9]\d*|[0]{1,1})$/;
            if (!reg.test(value)) {
                return false;
            } else {
                return true;
            }
        },
        //验证是否是数字0、正数和负整数
        input_NIsInit: function (value) {
            var reg = /^[-+]?([1-9]\d*|[0]{1,1})$/;
            if (!reg.test(value)) {
                return false;
            } else {
                return true;
            }
        },
        //验证数字大于0
        input_PIsNum: function (value) {
            if (isNaN(value)) {
                return false;
            } else {
                if (parseFloat(value) > 0) {
                    return true;
                } else {
                    return false;
                }
            }
        },
        //验证邮箱
        input_IsEmail: function (value) {
            var reg = /^[_a-zA-Z0-9_-_._-]+@([_a-zA-Z0-9_-]+\.)+[a-zA-Z]{2,3}$/;
            if (!reg.test(value)) {

                return false;
            } else {
                return true;
            }
        },
        //控制显示Model
        showModel: function (Arry_ModelId) {
            if ($('.app-model').is('.app-model-active')) {
                $('.app-model').removeClass('app-model-active');
            }
            $(Arry_ModelId).each(function (index, obj) {
                var Model = "#" + obj;
                $(Model).addClass('app-model-active');
            });
        },
        //控制按钮显示
        ChooseBtn: function (Arry_BtnId) {
            $('.app-btn-group').removeClass('disabled');

            $(Arry_BtnId).each(function (index, obj) {
                var btn = "#" + obj;
                $(btn).addClass('disabled');
            });
        },
        //获取选中的button value
        ChooseBtn_Index: function (id) {
            var str = id + " .app-btn-group.disabled"
            return $(str)[0].value;
        },
        //控制按钮可选
        btn_disabled: function (Arry_BtnId, disabled) {
            if (disabled === true) {
                $(Arry_BtnId).each(function (index, obj) {
                    var btn = "#" + obj;
                    $(btn).removeClass('layui-btn-disabled');
                    $(btn).attr("disabled", false);
                });
            } else {
                $(Arry_BtnId).each(function (index, obj) {
                    var btn = "#" + obj;
                    $(btn).addClass('layui-btn-disabled');
                    $(btn).attr("disabled", true);
                });
            }
        },
        //控制下拉可选
        Select_disabled: function (Arry_BtnId, disabled) {
            if (disabled === true) {
                $(Arry_BtnId).each(function (index, obj) {
                    var btn = "#" + obj;
                    $(btn).attr("disabled", false);
                });
            } else {
                $(Arry_BtnId).each(function (index, obj) {
                    var btn = "#" + obj;
                    $(btn).attr("disabled", true);
                });
            }
            form.render();
        },
        //标题显示
        show_title: function (id, text) {
            $('#' + id + '')[0].lastChild.data = text;
        },
        //开关按钮控制
        switch_render: function (Id, result) {
            var id = '#' + Id;
            if (result) {
                $(id)[0].value = 1;
                $(id).prop("checked", true);
            } else {
                $(id)[0].value = 0;
                $(id).prop("checked", false);
            }
            form.render("checkbox");
        },
        //输入框只读属性
        input_disabled: function (classeName) {
            $(classeName).attr("disabled", true);
            form.render();
        },
        input_undisabled: function (classeName) {
            $(classeName).attr("disabled", false);
            form.render();
        },
        form_verify: function () {
            form.verify({
                username: function (value, item) { //value：表单的值、item：表单的DOM对象
                    if (!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)) {
                        return '用户名不能有特殊字符';
                    }
                    if (/(^\_)|(\__)|(\_+$)/.test(value)) {
                        return '用户名首尾不能出现下划线\'_\'';
                    }
                    if (/^\d+\d+\d$/.test(value)) {
                        return '用户名不能全为数字';
                    }
                }
                , PIsNum: function (value, item) {
                    if (!/^[1-9]\d*$/.test(value)) {
                        return '请输入正整数';
                    }
                }
                //我们既支持上述函数式的方式，也支持下述数组的形式
                //数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
                , pass: [
                    /^[\S]{6,12}$/
                    , '密码必须6到12位，且不能出现空格'
                ]
            });
        }
    }
    //输出接口
    exports('common', obj);
});