﻿
/* Author:Javon Huang 2018/05/31 */
layui.define(['jquery', 'form', 'echarts'], function (exports) {
    var form = layui.form
        , Chart = layui.echarts
        , $ = layui.jquery;

    var obj = {
        EChartBarSimple: function (id, xAxis, yAxis, text, legend, name,color) {
            var myChart = Chart.init(document.getElementById(id));

            // 指定图表的配置项和数据
            var option = {
                color: color,
                title: {
                    text:text
                },
                tooltip: {},
                legend: {
                    data: [legend] //['销量']
                },
                xAxis: {
                    data: xAxis
                },
                yAxis: {},
                series: [{
                    name: name,//'销量'
                    type: 'bar',
                    data: yAxis
                }]
            };

            // 使用刚指定的配置项和数据显示图表。
            myChart.setOption(option);
        },
        EChart_yi: function (id, data1, data2, data3) {
            var myChart = Chart.init(document.getElementById(id));

            option = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: 'red'
                        }
                    }
                },
                toolbox: {
                    feature: {
                        dataView: { show: true, readOnly: false },
                        magicType: { show: true, type: ['line', 'bar'] },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                legend: {
                    data: ['订单量', '平均时效']
                },
                xAxis: [
                    {
                        type: 'category',
                        data: data1,
                        axisPointer: {
                            type: 'shadow'
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        name: '订单量/数量',
                        axisLabel: {
                            formatter: '{value} '
                        }
                    },
                    {
                        type: 'value',
                        name: '平均时效/分钟',
                        axisLabel: {
                            formatter: '{value}'
                        }
                    }
                ],
                series: [
                    {
                        name: '订单量',
                        type: 'bar',
                        data: data2,
                        itemStyle: {
                            normal: {
                                color: '#2db7f5'
                            }
                        }
                    },
                    {
                        name: '平均时效',
                        type: 'line',
                        yAxisIndex: 1,
                        data: data3
                    }
                ]
            };
            myChart.setOption(option);
        },

        EChart_keshi:function (id, data1, data2) {
            var myChart = Chart.init(document.getElementById(id));
            option = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: 'red'
                        }
                    }
                },
                toolbox: {
                    feature: {
                        dataView: { show: true, readOnly: false },
                        magicType: { show: true, type: ['line', 'bar'] },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                legend: {
                    data: ['订单量']
                },
                xAxis: [
                    {
                        type: 'category',
                        data: data1,
                        axisPointer: {
                            type: 'shadow'
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        name: '订单量/数量',
                        axisLabel: {
                            formatter: '{value} '
                        }
                    },
                ],
                series: [
                    {
                        name: '订单量',
                        type: 'bar',
                        // barWidth:10,
                        data: data2,
                        itemStyle: {
                            normal: {
                                color: new Chart.graphic.LinearGradient(0, 0, 0, 1, [{
                                    offset: 0,
                                    color: '#45CFFC'
                                }, {
                                    offset: 1,
                                    color: '#4CB1F6'
                                }]),
                            }
                        }
                    },
                    // {
                    //     name: '平均时效',
                    //     type: 'line',
                    //     yAxisIndex: 1,
                    //     data: data3
                    // }
                ]
            };
            myChart.setOption(option);

        },
        task_pre:function (id,data1,data2,data3) {
            var myChart = Chart.init(document.getElementById(id));
            option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    x: 'left',
                    data:['已完成','未开始','进行中']
                },
                series: [
                    {
                        name:'访问来源',
                        type:'pie',
                        radius: ['70%', '90%'],
                        avoidLabelOverlap: false,
                        label: {
                            normal: {
                                show: false,
                                position: 'center'
                            },
                            emphasis: {
                                show: true,
                                textStyle: {
                                    fontSize: '18',
                                    fontWeight: 'bold'
                                }
                            }
                        },
                        labelLine: {
                            normal: {
                                show: false
                            }
                        },
                        data:[
                            {value:data1, name:'已完成'},
                            {value:data2, name:'未开始'},
                            {value:data3, name:'进行中'},
                        ]
                    }
                ],
                color:['#3CC978', '#FFBB73','#FF5A50']

            };
            myChart.setOption(option);
        },
        emergency_pre:function (id,data1,data2) {
            var myChart = Chart.init(document.getElementById(id));
            option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    x: 'left',
                    data: ['已完成','未完成']
                },
                series: [
                    {
                        name:'访问来源',
                        type:'pie',
                        radius: ['70%', '90%'],
                        avoidLabelOverlap: false,
                        label: {
                            normal: {
                                show: false,
                                position: 'center'
                            },
                            emphasis: {
                                show: true,
                                textStyle: {
                                    fontSize: '18',
                                    fontWeight: 'bold'
                                }
                            }
                        },
                        labelLine: {
                            normal: {
                                show: false
                            }
                        },
                        data:[
                            {value:data1, name:'已完成'},
                            {value:data2, name:'未完成'},
                        ]
                    }
                ],
                color:['#0AA2E5', '#666666']

            };
            myChart.setOption(option);

        },
        pre_dailyEfficiency:function (id,data1,data2,data3,data4) {
            var myChart = Chart.init(document.getElementById(id));
            option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    x: 'left',
                    data: ['0-20分钟','20-40分钟','40-60分钟','其他']
                },
                series: [
                    {
                        name:'访问来源',
                        type:'pie',
                        radius: ['60%', '80%'],
                        avoidLabelOverlap: false,
                        label: {
                            normal: {
                                show: false,
                                position: 'center'
                            },
                            emphasis: {
                                show: true,
                                textStyle: {
                                    fontSize: '18',
                                    fontWeight: 'bold'
                                }
                            }
                        },
                        labelLine: {
                            normal: {
                                show: false
                            }
                        },
                        data:[
                            {value:data1, name:'0-20分钟'},
                            {value:data2, name:'20-40分钟'},
                            {value:data3, name:'40-60分钟'},
                            {value:data4, name:'其他'},
                        ]
                    }
                ],
                color:['#3CC978','#FFBB73', '#FF5A50','#9091E7']

            };
            myChart.setOption(option);
        },
        fiveDayComparison:function (id,data1,data2,data3) {
            var myChart = Chart.init(document.getElementById(id));
            option = {
                title : {
                    text: '今天和五天分类效能对比图',
                    x:'left'
                    // subtext: '纯属虚构'
                },
                tooltip : {
                    trigger: 'axis',
                     axisPointer: {
                         type: 'cross',
                             crossStyle: {
                             color: 'red'
                         }
                     }

                },

                toolbox: {
                    // show : true,
                    feature : {
                        dataView : {show: true, readOnly: false},
                        magicType : {show: true, type: ['line', 'bar']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                legend: {
                    data:['今天','五天']
                },
                // calculable : true,
                xAxis : [
                    {
                        type : 'category',
                        data : data1,
                        axisPointer: {
                            type: 'shadow'
                        }
                    }
                ],
                yAxis : [
                    {
                        type : 'value'
                    }
                ],
                series : [
                    {
                        name:'今天',
                        type:'bar',
                        data:data2,
                        markPoint : {
                        },
                        markLine : {
                        }
                    },
                    {
                        name:'五天',
                        type:'bar',
                        data:data3,
                        markPoint : {

                        },
                        markLine : {
                        }
                    }
                ]
            };
            myChart.setOption(option);
        }
    }
    //输出接口
    exports('myechart', obj);

});