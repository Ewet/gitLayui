var table, $, laydate, layer, common, element, rate, form, myechart;
layui.use(['element', 'rate', 'table', 'layer', 'common', 'form', 'laydate', 'formSelects', 'myechart'], function () {
    table = layui.table
        , $ = layui.jquery
        , laydate = layui.laydate
        , layer = layui.layer
        , common = layui.common
        , element = layui.element
        , rate = layui.rate
        , formSelects = layui.formSelects
        , myechart = layui.myechart
        , form = layui.form;
});

var app = new Vue({
    el: '#app',
    data: {
        tableins: {
            view_1_table_1_ins: null
        },
        timeperiod: {
            startTime: "",
            endTime:""
        },
        echartdata: {
            task_type: [],
            order_quantity: [],
            // average:[]
        }
    },
    created: function () {
        var that = this;
        setTimeout(function () {
            that.initpage();
        }, 1000);
    },
    methods: {
        initpage: function () {
            var that = this;
            common.showModel(['view_1']);
            that.initview_1_table_1();
            that.LoadChart();
            laydate.render({
                elem: '#stext' //指定元素
            });
            laydate.render({
                elem: '#sname' //指定元素
            });
        },
        initview_1_table_1: function () {
            var that = this;
            that._data.timeperiod.startTime = $('#stext').val();
            that._data.timeperiod.endTime = $('#sname').val();
            var index = layer.load(1, {
                shade: [0.1, '#fff'] //0.1透明度的白色背景
            });//转圈圈
            view_1_table_1_ins = table.render({
                elem: '#view_1_table_1'
                , url: common.ajax_g_url() + '/HRSSPortal/analysis/deptAnalysis'
                , method: "post"
                , cellMinWidth: 100
                ,where:{
                    startTime:that._data.timeperiod.startTime,
                    endTime:that._data.timeperiod.endTime,
                }
                , cols: [[
                    { type: 'checkbox' }
                    //             ,{field:'id', width:80, title: 'ID', sort: true}
                    , { field: 'deptCode', title: '科室编号' }
                    , { field: 'deptName', title: '科室名称', }
                    , { field: 'qty', title: '订单量' }
                    // , { field: 'avgTime', title: '平均时效' }
                    , {
                        field: '', title: '操作', align: 'center', templet: function (d) {
                            return '<a class="app-btn" lay-event="export" > <i class="iconfont icon-daochu"></i>导出</a>';
                        }
                    }

                ]]
                , done: function (res, curr, count) {
                    console.log(res);
                    table_data = res.data;
                    if (res.code != 0) {
                        $('.layui-none').html('暂无相关数据');
                    }
                    layer.close(index);
                }
            })
        },
        LoadChart: function () {
            var that = this;
            common.ajax_post('/HRSSPortal/analysis/deptAnalysis', { startTime: $('#stext').val(), endTime: $('#sname').val() }, true,
                function (data) {
                    that._data.echartdata.task_type=[];
                    that._data.echartdata.order_quantity=[];
                    for (var i = 0; i < data.data.length; i++) {
                        that._data.echartdata.task_type[i] = data.data[i].deptName;
                        that._data.echartdata.order_quantity[i] = data.data[i].qty;
                    }
                    myechart.EChart_keshi("task-echart", that._data.echartdata.task_type, that._data.echartdata.order_quantity);
                });
        },
        // refresh:function(){
        //     // alert(123);
        //     var that = this;
        //
        //     // console.log(that._data.timeperiod.startTime);
        //     that.initview_1_table_1();
        //     that.LoadChart();
        // }
    }
});