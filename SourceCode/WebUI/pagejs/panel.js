var table, $, laydate, layer, common, element, rate, form, myechart;
layui.use(['element', 'rate', 'table', 'layer', 'common', 'form', 'laydate', 'formSelects', 'myechart'], function () {
    table = layui.table
        , $ = layui.jquery
        , laydate = layui.laydate
        , layer = layui.layer
        , common = layui.common
        , element = layui.element
        , rate = layui.rate
        , formSelects = layui.formSelects
        , myechart = layui.myechart
        , form = layui.form;
});
var app = new Vue({
    el: '#app',
    data: {
        taskSum: "",
        predata: {
            Tasktype: [],
            Tasknumber: [],
            Tasktype: [],
            fiveDayComtype: [],
            fiveDayComnumber: [],
            todayComnumber: [],
        },
        taskStatus: {
            finished: "",
            acting: "",
            have_not_begum: "",
            sum: "",
        },
        dailyEfficienc: {
            firstTw: "",
            secondTw: "",
            thirdTw: "",
            other: "",

        },
        Emergency: {
            finished: "",
            unfinished: "",
        },
        orderdatalist: []
    },
    created: function () {
        var that = this;
        setTimeout(function () {
            that.initpage();
        }, 1000);
    },
    methods: {
        initpage: function () {
            var that = this;
            that.LoadChart();
            that.Emergency_pre();
            that.Task_sum();
            that.Task_number();
            that.dailyEfficiency();
            // that.fiveDayComparison();
            that.orderlist();
        },
        LoadChart: function () {
            var that = this;
            common.ajax_post('/HRSSPortal/analysis/dailyTaskStatusAnalysis', null, true,
                function (data) {
                    that._data.taskStatus.sum = data.data.sum;
                    that._data.taskStatus.have_not_begum = data.data.have_not_begum;
                    that._data.taskStatus.finished = data.data.finished;
                    that._data.taskStatus.acting = data.data.acting;
                    console.log(that._data.taskStatus.sum);
                    // console.log(data);
                    myechart.task_pre("task-pie", that._data.taskStatus.finished, that._data.taskStatus.have_not_begum, that._data.taskStatus.acting);
                })

        },
        Emergency_pre: function () {
            var that = this;
            common.ajax_post('/HRSSPortal/analysis/dailyEmergencyAnalysis', null, true,
                function (data) {
                    console.log(data.data);
                    that._data.Emergency.finished = data.data;
                    that._data.Emergency.unfinished = 1 - (data.data);

                    myechart.emergency_pre("Emergency-pie", that._data.Emergency.finished, that._data.Emergency.unfinished);
                })

        },
        Task_sum: function () {
            var that = this;
            common.ajax_post('/HRSSPortal/analysis/dailyTaskAnalysis', null, true,
                function (data) {
                    that._data.taskSum = data.data;
                    // console.log(data);
                })
        },
        Task_number: function () {
            var that = this;
            common.ajax_post('/HRSSPortal/analysis/dailyTaskAnalysis', null, true,
                function (data) {
                // console.log(data);
                    for (var i = 0; i < data.data.length; i++) {
                        that._data.predata.Tasktype[i] = data.data[i].descr;
                        that._data.predata.Tasknumber[i] = data.data[i].qty;
                    }
                    myechart.EChart_keshi("task_number", that._data.predata.Tasktype, that._data.predata.Tasknumber);
                });
        },
        dailyEfficiency: function () {
            var that = this;
            common.ajax_post('/HRSSPortal/analysis/dailyEfficiency', null, true,
                function (data) {
                    that._data.dailyEfficienc.firstTw = data.data["0-20分钟"];
                    that._data.dailyEfficienc.secondTw = data.data["20-40分钟"];
                    that._data.dailyEfficienc.thirdTw = data.data["40-60分钟"];
                    that._data.dailyEfficienc.other = data.data["其他"];
                    myechart.pre_dailyEfficiency("dailyEfficiency", that._data.dailyEfficienc.firstTw, that._data.dailyEfficienc.secondTw, that._data.dailyEfficienc.thirdTw, that._data.dailyEfficienc.other);
                });
        },
        // fiveDayComparison: function () {
        //     var that = this;
        //     common.ajax_post('/HRSSPortal/analysis/fiveDayComparison', null, true,
        //         function (data) {
        //             console.log(data);
        //             for (var i = 0; i < data.data.today.length; i++) {
        //                 that._data.predata.fiveDayComtype[i] = data.data.today[i].descr;
        //                 that._data.predata.todayComnumber[i] = data.data.today[i].avgTime;
        //             }
        //             for (var i = 0; i < data.data.fiveDay.length; i++) {
        //                 that._data.predata.fiveDayComnumber[i] = data.data.fiveDay[i].avgTime;
        //             }
        //             myechart.fiveDayComparison("fiveDay", that._data.predata.fiveDayComtype, that._data.predata.todayComnumber,that._data.predata.fiveDayComnumber);
        //         })
        // },
        orderlist: function () {
            var that = this;
            common.ajax_post('/HRSSPortal/order/getOrderWeb',{ "status": "1,2" }, true,
                function (data) {
                    if (data.code === 0) {
                        console.log(data.data);
                        var a = JSON.stringify(data.data[0]);
                        that._data.orderdatalist = data.data;
                    }
                });
        },
    }
})