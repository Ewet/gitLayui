﻿var table, $, laydate, layer, common, element, rate, form;
layui.use(['element', 'rate', 'table', 'layer', 'common', 'form', 'laydate'], function () {
    table = layui.table
        , $ = layui.jquery
        , laydate = layui.laydate
        , layer = layui.layer
        , common = layui.common
        , element = layui.element
        , rate = layui.rate
        , form = layui.form;
});

var app = new Vue({
    el: '#app',
    data: {
        qdata: {
            id: "",
            descr: ""
        },
        tableins: {
            view_1_table_1_ins: null
        },
        layerindex: {
            index_1: 1,
            index_2: 2,
            index_3: 3
        },
        tasktypenew: {
            descr: ""
        },
        tasktypeedit: {
            descr: "",
            taskTpId: ""
        }
    },
    created: function () {
        var that = this;
        setTimeout(function () {
            that.initpage();
        }, 1000);
    },
    methods: {
        initpage: function () {
            var that = this;
            that._data.layerindex.index_1 = layer.load(1, {
                shade: [0.1, '#fff'] //0.1透明度的白色背景
            });
            common.showModel(['view_1']);
            that._data.tableins.view_1_table_1_ins = table.render({
                elem: '#view_1_table_1'
                , url: common.ajax_g_url() + '/HRSSPortal/taskType/getData'
                , method: 'post'
                , where: {}
                , page: {
                    layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'] //自定义分页布局
                    , limit: 10
                    , groups: 5 //只显示 1 个连续页码
                    , first: true //不显示首页
                    , last: true //不显示尾页
                }
                , cellMinWidth: 100
                , cols: [[
                    { type: 'numbers' }
                    , { field: 'taskTpId', title: '类型编号', align: 'center', sort: true }
                    , { field: 'descr', title: '类型描述', minWidth: 110, align: 'center' }
                    , { field: 'updateBy', title: '修改者', minWidth: 110, align: 'center' }
                    , { field: 'updateTime', title: '修改日期', minWidth: 110, align: 'center' }
                    , {
                        field: '', title: '编辑', align: 'center',fixed: 'right',width:220, templet: function () {
                            return '<a class="app-btn" lay-event="edit"><i class="iconfont icon-bianji"></i>审核</a>' +
                                    '<a class="app-btn" lay-event="del" ><i class="iconfont icon-lajitong"></i> 删除</a>';
                        }
                    }
                ]]
                , done: function (res, curr, count) {
                    layer.close(that._data.layerindex.index_1);
                }
            });

            table.on('tool(view_1_table_1)', function (obj) {
                var data = obj.data;
                that._data.tasktypeedit.descr = data.descr;
                that._data.tasktypeedit.taskTpId = data.taskTpId;
                if (obj.event === 'edit') {
                    that._data.layerindex.index_3 = layer.open({
                        type: 1,
                        offset: 'auto',
                        shade: 0.1,
                        title: '<span><i class="iconfont icon-yonghuguanli" style="color:#3298cf"></i>&nbsp;编辑任务类型</span>',
                        content: $("#view_3"),
                        area: ["350px", "350px"]
                    });
                } else if (obj.event === "del") {
                    layer.confirm('真的删除行么', function (index) {
                        common.ajax_post('/HRSSPortal/taskType/delete', { "taskTpId":obj.data.taskTpId}, true,
                            function (data) {
                                console.log(data);
                                if (data.code == 0) {
                                    that.query();
                                    layer.close(index);
                                }
                            });
                    });
                }
            });

        },
        query: function () {
            var that = this;
            that._data.layerindex.index_1 = layer.load(1, {
                shade: [0.1, '#fff'] //0.1透明度的白色背景
            });
            table.reload('view_1_table_1', {
                where: {
                    descr: that._data.qdata.descr,
                    id: that._data.qdata.id
                }
            })
        },
        newtasktp: function () {
            var that = this;
            that._data.layerindex.index_2 = layer.open({
                type: 1,
                offset: 'auto',
                shade: 0.1,
                title: '<span><i class="iconfont icon-yonghuguanli" style="color:#3298cf"></i>&nbsp;新加任务类型</span>',
                content: $("#view_2"),
                area: ["350px", "350px"]
            });
        },
        savenew: function () {
            var that = this;
            if (that._data.tasktypenew.descr === "") {
                return;
            }
            common.ajax_post('/HRSSPortal/taskType/create', JSON.stringify(that._data.tasktypenew), true,
                function (data) {
                    console.log(data);
                    if (data.code == 0) {
                        that._data.tasktypenew.descr = "";
                        that.query();
                        layer.close(that._data.layerindex.index_2);
                    }
                })
        },
        canclenew: function () {
            var that = this;
            that._data.tasktypenew.descr = "";
            layer.close(that._data.layerindex.index_2);
        },
        saveedit: function () {
            var that = this;
            if (that._data.tasktypeedit.descr === "") {
                return;
            }
            common.ajax_post('/HRSSPortal/taskType/update', JSON.stringify(that._data.tasktypeedit), true,
                function (data) {
                    console.log(data);
                    if (data.code == 0) {
                        that._data.tasktypenew.descr = "";
                        that.query();
                        layer.close(that._data.layerindex.index_3);
                    }
                })
        },
        cancleedit: function () {
            var that = this;
            layer.close(that._data.layerindex.index_3);
        }
    }
})