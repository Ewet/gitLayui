﻿//alert(123);
layui.use(['layer', 'common', 'form'], function () {
    var $ = layui.jquery
        , layer = layui.layer
        , form = layui.form
        , common = layui.common

    var active = {
        Login: function () {
            var AccountID = $("#AccountID").val();
            var Password = $("#Password").val();
            //          console.log(hex_md5(Password));
            //          console.log(CryptoJS.SHA256(Password).toString());//密码加密处理

            //          console.log()
            if (AccountID === "" || Password === "") {
                layer.msg('账号或密码不可为空', {
                    icon: 5
                    , shade: 0.01
                    , time: 3000
                });
                return false;
            }
            var LoginIndx=layer.msg('登陆中...', {
                icon: 16
                , shade: 0.01
                ,time:20000
            });

            var detail = {
                accountId: AccountID,
                password: Password,
                hospitalId: 1
            }
            // 密码加密："password": CryptoJS.SHA256(Password).toString()
            common.ajax_post("/HRSSPortal/auth/login", JSON.stringify(detail), true,
                function (data) {
                console.log(data);
                    // var a = data;
                    if (data.msg === "SUCC") {
                        console.log(data);
                        var RememberMe = $("#RememberMe").prop("checked");
                        sessionStorage.setItem('RememberMe', RememberMe);
                        if (RememberMe) { sessionStorage.setItem('AccountID', AccountID); }
                        window.sessionStorage.setItem("userInfo", JSON.stringify(data.data));
                        layer.close(LoginIndx);
                        window.location.href = "portal.html";
                    } else {
                        layer.msg(data.message, { icon: 5 });
                    }
                });
        }
    };


    function page_Load() {
        var RememberMe = sessionStorage.getItem('RememberMe');

        if (RememberMe == "true" )
        {
            $("#AccountID").val(sessionStorage.getItem('AccountID'));
            $("#RememberMe").prop("checked", true)
        }
    }
    $('.app-login').on('click', function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });


    //  function CommitLogin(url, data) {
    //      common.ajax_post(url, data, true,
    //          function (data) {
    //          	console.log(data);
    //              if (data.code == "0") {
    //                  window.localStorage.setItem("userInfo", JSON.stringify(data.data.user));
    //                  sessionStorage.setItem("token", JSON.stringify(data.data.token));
    ////                  sessionStorage.setItem("username", JSON.stringify(data.data.userName));
    //                  window.location.href = "portal.html";                
    //              } else {
    //                  layer.msg("账号或密码错误", { icon: 5 });
    ////                  console.log(data.message);
    //              }
    //          }
    //      );
    //  }

    $(document).keydown(function (event) {
        if (event.which == 13) {
            active.Login();
        }
    });
});