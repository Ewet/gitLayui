﻿var table, $, laydate, layer, common, element, rate, form, formSelects;
layui.use(['element', 'rate', 'table', 'layer', 'common', 'form', 'laydate', 'formSelects'], function () {
    table = layui.table
        , $ = layui.jquery
        , laydate = layui.laydate
        , layer = layui.layer
        , common = layui.common
        , element = layui.element
        , rate = layui.rate
        , formSelects = layui.formSelects
        , form = layui.form;
});

var app = new Vue({
    el: '#app',
    data: {
        qdata: {
            search: '',
            deptCode: '',
            buildingName: '',
        },
        saveobj: {
            floorId: [],
            deptName: "",
            deptCode: "",
            deptId: 0
        },
        tableins: {
            view_1_table_1_ins: null
        },
        layerindex: {
            index_1: 1,
            index_2: 2,
            index_3:3
        },
        deptCode :[],
        operatetype: 0//0:新建;1:编辑
    },
    created: function () {
        var that = this;
        setTimeout(function () {
            that.initpage();
        }, 1000);
    },
    methods: {
        initpage: function () {
            var that = this;
            common.showModel(['view_1']);
            that._data.tableins.view_1_table_1_ins = table.render({
                elem: '#view_1_table_1'
                , url: common.ajax_g_url() + '/HRSSPortal/dept/getData'
                , where: JSON.stringify(that._data.qdata)
                , method: "post"
                , cellMinWidth: 100
                , cols: [[
                    { type: 'checkbox' }
                    , { field: 'deptCode', title: '科室编号' }
                    , { field: 'deptName', title: '科室名称', }
                    , { field: 'buildingName', title: '楼栋名称' }
                    , { field: 'floorName', title: '楼层' }
                    , { field: 'updateBy', title: '修改者' }
                    , { field: 'createTime', title: '修改日期' }
                    , {
                        field: '', title: '操作', align: 'center', templet: function (d) {
                            return '<a class="app-btn" lay-event="edit" > <i class="iconfont icon-bianji"></i>编辑</a>' +
                                '<a class="app-btn" lay-event="del" > <i class="iconfont icon-lajitong"></i>删除</a>' +
                                '<a class="app-btn" lay-event="export" > <i class="iconfont icon-daochu"></i>导出</a>';
                        }
                    }

                ]]
                , done: function (res, curr, count) {
                    layer.close(that._data.layerindex.index_1);
                }
            });
            table.on('tool(view_1_table_1)', function (obj) {
                var data = obj.data;
                if (obj.event === 'edit') {
                    that.showeditpage(data);
                } else if (obj.event === "del") {
                    that.del(data);
                }
            });
            common.SelectGroup("", { id: "buildingId", text: "buildingName" }, { child: "floor", id: "floorId", text: "floorName" }, "/HRSSPortal/floor/getTree", "post", null, "flood");
        },
        query: function () {
            var that = this;
            that._data.layerindex.index_1 = layer.load(1, {
                shade: [0.1, '#fff'] //0.1透明度的白色背景
            });
            that._data.tableins.view_1_table_1_ins.reload({
                where: that._data.qdata
            });
        },
        //弹框的函数
        shownewpage: function () {
            var that = this;
            that._data.operatetype = 0;
            that._data.saveobj.floorId = [];
            that._data.layerindex.index_2 = layer.open({
                type: 1,
                offset: 'auto',
                shadeClose: true,
                shade: 0.1,
                title: '<span><i class="iconfont icon-yonghuguanli" style="color:#3298cf"></i>&nbsp;添加科室</span>',
                content: $("#view_2"),
                area: ["950px", "500px"]
            });
            //console.log(1);
        },
        showeditpage: function (d) {
            var that = this;
            that._data.saveobj.floorId = [];
            that._data.operatetype = 1;
            that._data.saveobj.floorId.push(d.floorId);
            that._data.saveobj.deptName = d.deptName;
            that._data.saveobj.deptCode = d.deptCode;
            that._data.saveobj.deptId = d.deptId;           
            that._data.layerindex.index_2 = layer.open({
                type: 1,
                offset: 'auto',
                shadeClose: true,
                shade: 0.1,
                title: '<span><i class="iconfont icon-yonghuguanli" style="color:#3298cf"></i>&nbsp;添加科室</span>',
                content: $("#view_2"),
                area: ["950px", "500px"]
            });           
            common.Select_render([d.floorId], "flood");
        },
        save: function () {
            var that = this;
            that._data.saveobj.floorId.push($("#flood").val());
            if (that._data.operatetype === 0) {
                common.ajax_post('/HRSSPortal/dept/create', JSON.stringify(that._data.saveobj), true,
                    function (data) {
                        if (data.code === 0) {
                            layer.msg(data.msg, { icon: 1 });
                            layer.close(that._data.layerindex.index_2);
                        } else {
                            layer.msg(data.msg, { icon: 5 });
                        }
                    });
            } else {
                common.ajax_post('/HRSSPortal/dept/update', JSON.stringify(that._data.saveobj), true,
                    function (data) {
                        if (data.code === 0) {
                            layer.msg(data.msg, { icon: 1 });
                            layer.close(that._data.layerindex.index_2);
                        } else {
                            layer.msg(data.msg, { icon: 5 });
                        }
                    });
            }           
        },
        del: function (d) {
            var that = this;
            var confirm = layer.confirm('请确认删除！', {
                btn: ['确定', '取消'] //按钮
                , title: '提示',
            }, function () {
                common.ajax_post('/HRSSPortal/dept/delete', { "deptId": d.deptId}, true,
                    function (data) {
                        if (data.code === 0) {
                            layer.msg(data.msg, { icon: 1 });
                            layer.close(that._data.layerindex.index_2);
                        } else {
                            layer.msg(data.msg, { icon: 5 });
                        }
                    });
            }, function () {
                layer.close();
            });
        },
        print: function () {
            var that = this;
            var checkdata = table.checkStatus('view_1_table_1').data; 
            that._data.layerindex.index_3 = layer.open({
                type: 1,
                offset: 'auto',
                shadeClose: true,
                shade: 0.1,
                title: '<span><i class="iconfont icon-yonghuguanli" style="color:#3298cf"></i>&nbsp;二维码</span>',
                content: $("#view_3"),
                area: ["950px", "500px"],
                cancel: function(){
                    $("#printarea div").remove();
                    that.initpage();
                }
            });
            $('p.app-toptitle').hide();
            $.each(checkdata, function (index, item) {
                var oDiv = document.createElement('div');
                oDiv.id = index;
                oDiv.className = 'print';
                document.getElementById('printarea').appendChild(oDiv);
                // var aa = item.deptId;
                var qrcode = new QRCode(document.getElementById(index), {
                    text: item.deptId.toString() ,
                    width: 350, //生成的二维码的宽度
                    height: 350, //生成的二维码的高度
                    // marginHeight: 10,
                    colorDark: "#000000", // 生成的二维码的深色部分
                    colorLight: "#ffffff", //生成二维码的浅色部分
                    correctLevel: QRCode.CorrectLevel.H
                });
                // console.log(item.deptId)
            });
        }
    }
});