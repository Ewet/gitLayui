﻿var table, $, laydate, layer, common, element, rate, form, formSelects;
layui.use(['element', 'rate', 'table', 'layer', 'common', 'form', 'laydate', 'formSelects'], function () {
    table = layui.table
        , $ = layui.jquery
        , laydate = layui.laydate
        , layer = layui.layer
        , common = layui.common
        , element = layui.element
        , rate = layui.rate
        , formSelects = layui.formSelects
        , form = layui.form;
});

var app = new Vue({
    el: '#app',
    data: {
        qdata: {
            search: '',
            id: '',
        },
        saveobj: {
            buildingName:"",
            hospitalId:"",
            buildingId:""
        },
        tableins: {
            view_1_table_1_ins: null
        },
        layerindex: {
            index_1: 1,
            index_2: 2,
            index_3:3
        },
        operatetype:"",
        isdisabled:false
    },
    created: function () {
        var that = this;
        setTimeout(function () {
            that.initpage();
        }, 1000);
    },
    methods: {
        initpage: function () {
            var that = this;
            common.showModel(['view_1']);
            that._data.tableins.view_1_table_1_ins = table.render({
                elem: '#view_1_table_1'
                , url: common.ajax_g_url() + '/HRSSPortal/building/getData'
                , where: that._data.qdata
                , method: "post"
                // , page: {
                //     layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'] //自定义分页布局
                //     , limit: 10
                //     , groups: 5 //只显示 1 个连续页码
                //     , first: '首页' //不显示首页
                //     , last: '尾页' //不显示尾页
                // }
                , cellMinWidth: 100
                , cols: [[
                    { type: 'checkbox' }
                    , { field: 'buildingId', title: '楼栋编号' }
                    , { field: 'buildingName', title: '楼栋名称', }
                    , { field: 'updateBy', title: '修改者' }
                    , { field: 'updateTime', title: '修改日期' }
                    , {
                        field: '', title: '操作', align: 'center',fixed: 'right',width:220, templet: function (d) {
                            return '<a class="app-btn" lay-event="edit" > <i class="iconfont icon-bianji"></i>编辑</a>' +
                                '<a class="app-btn" lay-event="del" > <i class="iconfont icon-lajitong"></i>删除</a>' +
                                '<a class="app-btn" lay-event="export" > <i class="iconfont icon-daochu"></i>导出</a>';
                        }
                    }

                ]]
                , done: function (res, curr, count) {
                    layer.close(that._data.layerindex.index_1);
                }
            });
            table.on('tool(view_1_table_1)', function (obj) {
                var data = obj.data;
                if (obj.event === 'edit') {
                    that.showeditpage(data);
                    // console.log(data);
                } else if (obj.event === "del") {
                    that.del(data);
                }
            });
        },
        query: function () {
            var that = this;
            that._data.layerindex.index_1 = layer.load(1, {
                shade: [0.1, '#fff'] //0.1透明度的白色背景
            });
            that._data.tableins.view_1_table_1_ins.reload({
                where: that._data.qdata
            });
        },
        shownewpage: function () {
            var that = this;
            that._data.operatetype = 0;
            that._data.isdisabled = false;
            that._data.saveobj.buildingName = "";
            that._data.saveobj.hospitalId = "";
            that._data.saveobj.buildingId = "";
            // that._data.saveobj.floorId = [];
            that._data.layerindex.index_2 = layer.open({
                type: 1,
                offset: 'auto',
                shadeClose: true,
                shade: 0.1,
                title: '<span><i class="iconfont icon-yonghuguanli" style="color:#3298cf"></i>&nbsp;添加楼栋</span>',
                content: $("#view_2"),
                area: ["550px", "350px"]
            });
        },
        showeditpage: function (d) {
            var that = this;
            that._data.operatetype = 1;
            that._data.isdisabled = true;
            that._data.saveobj.buildingId = d.buildingId;
            that._data.saveobj.buildingName = d.buildingName;
            that._data.saveobj.hospitalId = d.hospitalId;
            that._data.layerindex.index_2 = layer.open({
                type: 1,
                offset: 'auto',
                shadeClose: true,
                shade: 0.1,
                title: '<span><i class="iconfont icon-yonghuguanli" style="color:#3298cf"></i>&nbsp;编辑楼栋</span>',
                content: $("#view_2"),
                area: ["550px", "350px"]
            });
            // common.Select_render([d.floorId], "flood");
        },
        save: function () {
            var that = this;
            // that._data.saveobj.floorId.push($("#flood").val());
            if (that._data.operatetype === 0) {
                common.ajax_post('/HRSSPortal/building/create', JSON.stringify(that._data.saveobj), true,
                    function (data) {
                        if (data.code === 0) {
                            layer.msg(data.msg, { icon: 1 });
                            layer.close(that._data.layerindex.index_2);
                            // that._data.tableins.view_1_table_1_ins;
                            that.refresh();
                        } else {
                            layer.msg(data.msg, { icon: 5 });
                        }
                    });
            } else {
                common.ajax_post('/HRSSPortal/building/update', JSON.stringify(that._data.saveobj), true,
                    function (data) {

                        if (data.code === 0) {
                            layer.msg(data.msg, { icon: 1 });
                            layer.close(that._data.layerindex.index_2);
                            that.refresh();
                        } else {
                            layer.msg(data.msg, { icon: 5 });
                        }
                    });
            }
        },
        del: function (d) {
            var that = this;
            var confirm = layer.confirm('请确认删除！', {
                btn: ['确定', '取消'] //按钮
                , title: '提示',
            }, function () {
                common.ajax_post('/HRSSPortal/building/delete', { "buildingId": d.buildingId}, true,
                    function (data) {
                        if (data.code === 0) {
                            layer.msg(data.msg, { icon: 1 });
                            layer.close(that._data.layerindex.index_2);
                            that.refresh();
                        } else {
                            layer.msg(data.msg, { icon: 5 });
                        }
                    });
            }, function () {
                layer.close();
            });
        },
        refresh:function () {
            var that = this;
            that._data.tableins.view_1_table_1_ins.reload({
                where: that._data.qdata
            });
        }



    }
});