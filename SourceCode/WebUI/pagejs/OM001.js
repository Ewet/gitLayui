﻿var table, $, laydate, layer, common, element, rate, form, formSelects;
layui.use(['element', 'rate', 'table', 'layer', 'common', 'form', 'laydate', 'formSelects'], function () {
    table = layui.table
        , $ = layui.jquery
        , laydate = layui.laydate
        , layer = layui.layer
        , common = layui.common
        , element = layui.element
        , rate = layui.rate
        , formSelects = layui.formSelects
        , form = layui.form;
});

var app = new Vue({
    el: '#app',
    data: {
        qdata: {
            id: "",
            descr: ""
        },
        tableins: {
            view_1_table_1_ins: null
        },
        layerindex: {
            index_1: 1,
            index_2: 2,
            index_3: 3
        },
        tasktypeobj: {
            taskTpId: "",
            descr: "",
            task: [],
            orderConfig: []
        },
        orderConfiglist_OT01: {
            orderType: "",
            startType: "",
            handoverType: []
        },
        orderConfiglist_OT02: {
            orderType: "",
            startType: "",
            handoverType: []
        },
        orderConfiglist_OT03: {
            orderType: "",
            startType: "",
            handoverType: []
        },
        orderConfiglist_OT04: {
            orderType: "",
            startType: "",
            handoverType: []
        },
        orderConfiglist: [],
        newdescrtask: "",
        operatetype: 0//0:新建;1:编辑
    },
    created: function () {
        var that = this;
        setTimeout(function () {
            that.initpage();
        }, 1000);
    },
    methods: {
        initpage: function () {
            var that = this;
            that._data.layerindex.index_1 = layer.load(1, {
                shade: [0.1, '#fff'] //0.1透明度的白色背景
            });
            common.showModel(['view_1']);
            that._data.tableins.view_1_table_1_ins = table.render({
                elem: '#view_1_table_1'
                , url: common.ajax_g_url() + '/HRSSPortal/taskType/getData'
                , method: 'post'
                , where: {}
                , page: {
                    layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'] //自定义分页布局
                    , limit: 10
                    , groups: 5 //只显示 1 个连续页码
                    , first: true //不显示首页
                    , last: true //不显示尾页
                }
                , cellMinWidth: 100
                , cols: [[
                    { type: 'numbers' }
                    // , { field: 'taskTpId', title: '类型编号', align: 'center', sort: true }
                    , { field: 'descr', title: '类型描述', minWidth: 110, align: 'center' }
                    , { field: 'createBy', title: '创建者', minWidth: 110, align: 'center' }
                    , { field: 'createTime', title: '创建日期', minWidth: 110, align: 'center' }
                    , {
                        field: '', title: '编辑', align: 'center', templet: function (d) {

                            return '<a class="app-btn" lay-event="edit" > <i class="iconfont icon-bianji"></i>编辑</a>' +
                                '<a class="app-btn" lay-event="del" > <i class="iconfont  icon-lajitong"></i>删除</a>';
                        }
                    }
                ]]
                , done: function (res, curr, count) {
                    layer.close(that._data.layerindex.index_1);
                }
            });

            table.on('tool(view_1_table_1)', function (obj) {
                var data = obj.data;
                that._data.tasktypeobj.taskTpDescr = data.descr;
                that._data.tasktypeobj.taskTpId = data.taskTpId;
                if (obj.event === 'edit') {
                    that._data.layerindex.index_2 = layer.open({
                        type: 1,
                        offset: 'auto',
                        shade: 0.1,
                        title: '<span><i class="iconfont icon-yonghuguanli" style="color:#3298cf"></i>&nbsp;编辑任务类型</span>',
                        content: $("#view_2"),
                        area: ["950px", "500px"]
                    });
                    that.edittasktp();
                } else if (obj.event === "del") {
                    layer.confirm('确定删除该信息吗', function (index) {
                        common.ajax_post('/HRSSPortal/taskType/delete', { "taskTpId": obj.data.taskTpId }, true,
                            function (data) {
                                console.log(data);
                                if (data.code == 0) {
                                    that.query();
                                    layer.close(index);
                                }
                            });
                    });
                }
            });

        },
        query: function () {
            var that = this;
            that._data.layerindex.index_1 = layer.load(1, {
                shade: [0.1, '#fff'] //0.1透明度的白色背景
            });
            table.reload('view_1_table_1', {
                where: {
                    descr: that._data.qdata.descr,
                    id: that._data.qdata.id
                }
            })
        },
        newtasktp: function () {
            var that = this;
            that._data.layerindex.index_2 = layer.open({
                type: 1,
                offset: 'auto',
                shade: 0.1,
                title: '<span><i class="iconfont icon-yonghuguanli" style="color:#3298cf"></i>&nbsp;新加任务类型</span>',
                content: $("#view_2"),
                area: ["950px", "500px"]
            });
            that._data.tasktypeobj.taskTpId = "";
            that._data.tasktypeobj.taskTpDescr = "";
            that._data.tasktypeobj.task = [];
            that._data.tasktypeobj.orderConfig = [];
            that._data.operatetype = 0;
            that._data.tasktypeobj.descr = "";
            that.cleanorderConfiglist();
        },
        edittasktp: function () {
            var that = this;
            that._data.operatetype = 1;
            common.ajax_post('/HRSSPortal/task/getTaskTypeAndConfig', { "id": that._data.tasktypeobj.taskTpId }, true,
                function (data) {
                    console.log(data);
                    if (data.code === 0) {
                        var b = JSON.stringify(data.data);
                        that._data.tasktypeobj.taskTpId = data.data[0].taskTpId;
                        that._data.tasktypeobj.descr = data.data[0].descr;
                        that._data.tasktypeobj.orderConfig = data.data[0].orderConfig;
                        that._data.tasktypeobj.task = data.data[0].task;

                        $.each(data.data[0].orderConfig, function (index, item) {
                            if (item.orderType === "OT01") {
                                that._data.orderConfiglist_OT01.orderType = "OT01";
                                that._data.orderConfiglist_OT01.startType = item.startType;
                                that._data.orderConfiglist_OT01.handoverType.push(item.handoverType);
                            } else if (item.orderType === "OT02") {
                                that._data.orderConfiglist_OT02.orderType = "OT02";
                                that._data.orderConfiglist_OT02.startType = item.startType;
                                that._data.orderConfiglist_OT02.handoverType.push(item.handoverType);
                            } else if (item.orderType === "OT03") {
                                that._data.orderConfiglist_OT03.orderType = "OT03";
                                that._data.orderConfiglist_OT03.startType = item.startType;
                                that._data.orderConfiglist_OT03.handoverType.push(item.handoverType);
                            } else if (item.orderType === "OT04") {
                                that._data.orderConfiglist_OT04.orderType = "OT04";
                                that._data.orderConfiglist_OT04.startType = item.startType;
                                that._data.orderConfiglist_OT04.handoverType.push(item.handoverType);
                            }
                        });
                        that.initorderConfiglist();
                    }
                });
        },
        cancletask: function () {
            var that = this;
            layer.close(that._data.layerindex.index_2);
        },
        delitem: function (d, index) {
            var that = this;
            that._data.tasktypeobj.task;
            d.action = "D";
            Vue.set(that._data.tasktypeobj, index, { action: 'D' });
        },
        addnewtask: function () {
            var that = this;
            if (that._data.newdescrtask === "") {
                return;
            }
            that._data.tasktypeobj.task.push({ action: "N", descr: that._data.newdescrtask });
            that._data.newdescrtask = "";
        },
        savenewtask: function () {
            var that = this;
            that._data.orderConfiglist = [];
            that._data.orderConfiglist_OT01.handoverType = formSelects.value('OT01_handoverType', 'val');
            that._data.orderConfiglist_OT02.handoverType = formSelects.value('OT02_handoverType', 'val');
            that._data.orderConfiglist_OT03.handoverType = formSelects.value('OT03_handoverType', 'val');
            that._data.orderConfiglist_OT04.handoverType = formSelects.value('OT04_handoverType', 'val');

            that._data.orderConfiglist_OT01.startType = $("#OT01_startType").val();
            that._data.orderConfiglist_OT02.startType = $("#OT02_startType").val();
            that._data.orderConfiglist_OT03.startType = $("#OT03_startType").val();
            that._data.orderConfiglist_OT04.startType = $("#OT04_startType").val();


            $.each(that._data.orderConfiglist_OT01.handoverType, function (index, item) {
                that._data.orderConfiglist.push({ orderType: "OT01", startType: that._data.orderConfiglist_OT01.startType, handoverType: item });
            });

            $.each(that._data.orderConfiglist_OT02.handoverType, function (index, item) {
                that._data.orderConfiglist.push({ orderType: "OT02", startType: that._data.orderConfiglist_OT02.startType, handoverType: item });
            });

            $.each(that._data.orderConfiglist_OT03.handoverType, function (index, item) {
                that._data.orderConfiglist.push({ orderType: "OT03", startType: that._data.orderConfiglist_OT03.startType, handoverType: item });
            });

            $.each(that._data.orderConfiglist_OT04.handoverType, function (index, item) {
                that._data.orderConfiglist.push({ orderType: "OT04", startType: that._data.orderConfiglist_OT04.startType, handoverType: item });
            });
            that._data.tasktypeobj.orderConfig = that._data.orderConfiglist;

            if (that._data.operatetype === 1) {
                common.ajax_post('/HRSSPortal/task/updateTaskAndConfig', JSON.stringify(that._data.tasktypeobj), true,
                    function (data) {
                        if (data.code === 0) {
                            layer.msg(data.msg, { icon: 1 });
                            that.query();
                            layer.close(that._data.layerindex.index_2);
                        } else {
                            layer.msg(data.msg, { icon: 5 });
                        }
                    });
            } else {
                common.ajax_post('/HRSSPortal/task/createTaskAndConfig', JSON.stringify(that._data.tasktypeobj), true,
                    function (data) {
                        if (data.code === 0) {
                            that.query();
                            layer.msg(data.msg, { icon: 1 });
                            layer.close(that._data.layerindex.index_2);
                        } else {
                            layer.msg(data.msg, { icon: 5 });
                        }
                    });
            }
        },
        initorderConfiglist: function () {
            var that = this;
            common.Select_render([], "OT01_startType");
            common.Select_render([], "OT02_startType");
            common.Select_render([], "OT03_startType");
            common.Select_render([], "OT04_startType");

            common.Select_render([that._data.orderConfiglist_OT01.startType], "OT01_startType");
            formSelects.value('OT01_handoverType', that._data.orderConfiglist_OT01.handoverType);
            common.Select_render([that._data.orderConfiglist_OT02.startType], "OT02_startType");
            formSelects.value('OT02_handoverType', that._data.orderConfiglist_OT02.handoverType);
            common.Select_render([that._data.orderConfiglist_OT03.startType], "OT03_startType");
            formSelects.value('OT03_handoverType', that._data.orderConfiglist_OT03.handoverType);
            common.Select_render([that._data.orderConfiglist_OT04.startType], "OT04_startType");
            formSelects.value('OT04_handoverType', that._data.orderConfiglist_OT04.handoverType);
        },
        cleanorderConfiglist: function () {
            var that = this;
            common.Select_render([], "OT01_startType");
            common.Select_render([], "OT02_startType");
            common.Select_render([], "OT03_startType");
            common.Select_render([], "OT04_startType");

            common.Select_render([], "OT01_startType");
            formSelects.value('OT01_handoverType', []);
            common.Select_render([], "OT02_startType");
            formSelects.value('OT02_handoverType', []);
            common.Select_render([], "OT03_startType");
            formSelects.value('OT03_handoverType', []);
            common.Select_render([], "OT04_startType");
            formSelects.value('OT04_handoverType', []);
        }
    }
})