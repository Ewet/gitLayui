var table, $, laydate, layer, common, element, rate, form, formSelects;
layui.use(['element', 'rate', 'table', 'layer', 'common', 'form', 'laydate', 'formSelects'], function () {
    table = layui.table
        , $ = layui.jquery
        , laydate = layui.laydate
        , layer = layui.layer
        , common = layui.common
        , element = layui.element
        , rate = layui.rate
        , formSelects = layui.formSelects
        , form = layui.form;
    // alert(123);
});
var app = new Vue({
    el: '#app',
    data: {
        depmanagelist: "",
        tstype: "",
        worktype: "",
        taskTpId: "",
        isA: false,
        begin: "",
        beginid: "",
        end: "",
        endid: "",
        stype: "",
        stypeid: [],
        current: "",
        currentid: "",
        remark: "",
        current:null,
        current1:null,
        current2:null,
        current3:null,
        current5:null,
        currenttype:null,
        current4:0,
        oneArea:"未选择",
        twoArea:"未选择",
        changeaddress:"选择起点",
        result:"",
        // bolle:false,
        newtaskorder: {
            orderType: "",
            origin: "",
            taskTpId: 0,
            qty: 0,
            target: 0,
            taskId: [],
            toolId: null,
            assigeTo: "",
            remark: "",
            priority: 10,
            planEndTime: ""
        },
        assigeuserlist: "",
        assigeuser:"",
        status:[],
        address:[{
            addre:"起点",
            iconfont:"iconfont icon-3-copy"
        },{
            addre:"终点",
            iconfont:"iconfont icon-zhongdian"
        }],
        check:[{
            name:"是",
        },{
            name:"否"
        }]
    },
    created: function () {
        var that = this;
        setTimeout(function () {
            that.initpage();
        }, 1000);
    },
    methods: {
        initpage: function () {
            var that = this;
            that.orderassign();
            that.Tasktype();
            that.gettool();
            // that.Typetion();
            form.on('checkbox(priority)', function (data) {
                if (this.checked) {
                    that._data.newtaskorder.priority = 20;
                } else {
                    that._data.newtaskorder.priority = 10;
                }
            });

        },
        orderassign: function () {
            var that = this;
            common.ajax_post('/HRSSPortal/dept/getData', null, true,
                function (data) {
                    console.log(data);
                    if (data.code === 0) {
                        that._data.depmanagelist = data.data;
                    }
                });
        },
        Tasktype: function () {
            var that = this;
            common.ajax_post('/HRSSPortal/taskType/getData', null, true,
                function (data) {
                    console.log(data);
                    if (data.code === 0) {
                        that._data.tstype = data.data;
                    }
                });
        },
        task: function () {
            var that = this;
            common.ajax_post('/HRSSPortal/task/getData', { taskTpId: that._data.taskTpId }, true,
                function (data) {
                    console.log(data);
                    if (data.code === 0) {
                        that._data.worktype = data.data;
                        for(var i=0;i<that._data.worktype.length;i++){
                            that._data.status[i]=false;
                        }
                    }

                });
        },
        gettool: function () {
            common.ajax_post('/HRSSPortal/trantool/getDataTbtrantool', null, true,
                function (data) {
                    if (data.code === 0) {
                        var a = common.formSelects2("", { id: "tool_id", text: "descr" }, data.data);
                        formSelects.data('toolId', 'local', a);
                    }
                });
        },
        chance: function (d, index) {
            var that = this;
            that._data.taskTpId = d.taskTpId;
            that.task();
            that._data.current = index;
            that._data.stypeid=[];
        },
        beginArea: function (d,index) {
            var that = this;
            that._data.oneArea = d.deptName;
            that._data.beginid = d.deptId;
            that._data.current1=index;
            that.getorderassign(d.deptId);
        },
        beginArea1:function(d,index){
            var that = this;
            that._data.current2=index;
        },
        change:function(d,index){
            var that = this;
            that._data.current4 = index;
            if(index==0){
                that._data.changeaddress="选择起点"
            }else{
                that._data.changeaddress="选择终点"
            }
            // console.log(d,index);
        },
        checking:function(d,index){
            var that = this;
            that._data.current3=index;

        },
        endArea: function (d,index) {
            var that = this;
            that._data.twoArea = d.deptName;
            that._data.endid = d.deptId;
            that._data.current5 = index;

        },
        sendtype: function (d,index) {
            var that = this;
            that._data.stype = d.descr;
            // that._data.stypeid = d.taskId;
            that._data.currenttype=index;
            that._data.result= !that._data.status[index];
            that._data.result == true?that._data.stypeid.push(d.taskId):that._data.stypeid =that._data.stypeid.filter(i => {return i != d.taskId})
            console.log(that._data.stypeid);
            Vue.set(that._data.status, index, that._data.result )
        },
        saveneworder: function () {
            var that = this;
            that._data.newtaskorder.planEndTime = common.nowDate().shortDate+" 00:00:00";
            that._data.newtaskorder.taskTpId = that._data.taskTpId;
            that._data.newtaskorder.orderType = "OT01";
            that._data.newtaskorder.taskId = that._data.stypeid;
            that._data.newtaskorder.toolId = formSelects.value('toolId', 'val');
            that._data.newtaskorder.remark = "";
            that._data.newtaskorder.priority = $("#priority").val();
            that._data.newtaskorder.origin = that._data.beginid;
            that._data.newtaskorder.target = that._data.endid;
            console.log(that._data.newtaskorder.taskId);
            var a = JSON.stringify(that._data.newtaskorder);
            common.ajax_post('/HRSSPortal/order/CreateDispatchOrder', JSON.stringify(that._data.newtaskorder), true,
                function (data) {
                    if (data.code === 0) {
                        layer.msg(data.msg, { icon: 1 });
                        setTimeout(function () {
                            location.reload();
                        }, 1000);
                    } else {
                        layer.msg(data.msg, { icon: 5 });
                    }
                });
        },
        getorderassign: function (d,index) {
            var that = this;
            that._data.current2=index;
            common.ajax_post('/HRSSPortal/order/getShipper', {"deptId":d}, true,
                function (data) {
                    console.log(data);
                    if (data.code === 0) {
                        that._data.assigeuserlist = data.data;
                    }
                });
        },
        setassign: function (d) { 
            var that = this;
            that._data.newtaskorder.assigeTo = d.userId;
            that._data.assigeuser = d.userName;
        }
    }


})