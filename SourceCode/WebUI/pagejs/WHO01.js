﻿layui.use(['table', 'layer', 'common', 'form', 'laydate'], function () {
    var $ = layui.jquery
        , laydate = layui.laydate
        , layer = layui.layer
        , common = layui.common
        , form = layui.form
        , table = layui.table;
    var WhLocInvTableIns = null;
    page_Load();

    var active = {
        Query: function () {
            var QueryData = ReturnQuery();
            initWhLocInvTable(QueryData.WHCD, QueryData.Location, QueryData.PN);
        },
        export: function () {
            common.ajax_post("/api/InvlocAPI/GetCacheInv", null, true,
                function (data) {
                    if (data.Result === "success") {
                        common.execl_export_render(data.data, "暂存区库存", "xlsx", [["WHCDescr", "库房描述"], ["WHCD", "库房编号"], ["PNDescr", "物料描述"], ["PN", "物料"], ["LocationDescr", "库位描述"], ["Location", "库位编号"], ["Avail_Qty", "可用数量"], ["Resv_Qty", "冻结数量"], ["Qty", "数量"], ["ModifyName", "修改人"], ["ModifyDate", "修改时间"]], ["ModifyBy"]);
                    } else {
                        layer.msg(data.message, { icon: 5 });
                    }
                });
        }
    }

    function ReturnQuery() {
        return {
            "WHCD": $.trim($('#QWHCD').val()),
            "PN": $.trim($('#QPN').val()),
            "Location": $.trim($('#QLocation').val())
        }
    }

    $('.operate .layui-btn-sm').on('click', function () {
        var othis = $(this), method = othis.data('method');
        active[method] ? active[method].call(this, othis) : '';
    });

    function page_Load() {
        initWhLocInvTable();
    }

    function initWhLocInvTable(WHCD, Location, PN) {
        var index = layer.load(1, {
            shade: [0.1, '#fff']
        });
        WhLocInvTableIns = table.render({
            elem: '#WhLocInvTable'
            , url: '/api/InvlocAPI/GetCacheInv'
            , method: 'post'
            , where: { WHCD: WHCD, Location: Location, PN: PN, blPage: "true" }
            , page: {
                layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'] //自定义分页布局
                , limit: 10
                , groups: 1 //只显示 1 个连续页码
                , first: false //不显示首页
                , last: false //不显示尾页
            }
            , cellMinWidth: 100
            , cols: [[
                { type: 'numbers' }
                , { field: 'WHCD', title: '库房编号', unresize: true, sort: true, align: 'center', width: 160 }
                , { field: 'WHCDescr', title: '库房描述', align: 'center' }
                , { field: 'Location', title: '库位编号', align: 'center' }
                , { field: 'LocationDescr', title: '库位描述', minWidth: 110, align: 'center' }
                , { field: 'PN', title: '物料', align: 'center' }
                , { field: 'PNDescr', title: '物料描述', align: 'center' }
                , { field: 'Qty', title: '数量', align: 'center' }
                , { field: 'Avail_Qty', title: '可用数量', align: 'center' }
                , { field: 'Resv_Qty', title: '冻结数量', align: 'center' }
                , { field: 'ModifyName', title: '修改人', align: 'center' }
                , { field: 'ModifyDate', title: '修改时间', align: 'center' }
            ]]
            , done: function (res, curr, count) {
                layer.close(index);
            }
        });
    }


});