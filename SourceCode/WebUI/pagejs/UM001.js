layui.use(['element', 'rate', 'table', 'layer', 'common', 'form', 'laydate'], function () {
    var table = layui.table
        , $ = layui.jquery
        , laydate = layui.laydate
        , layer = layui.layer
        , common = layui.common
        , element = layui.element
        , rate = layui.rate
        , form = layui.form;

    // var userinfo = JSON.parse(localStorage.getItem("userInfo"));
    var view_1_table_1_ins = null;
    var  morenId;
    var ids = new Array();
    var ids1 = new Array();
    var ids2;
    var table_data=new Array();
    var node = document.getElementById('save');

    var active_veiw_1 = {
        ShowPage: function () {
            // common.show_title('title', '查询条件');
            common.showModel(['view_1']);
            initview_1_table_1();
        },
        Query: function () {
            // console.log($('.stext').val(),$('.sname').val());
            table.reload('view_1_table_1', {
                where:{
                    search:$('.sname').val(),
                    id:$('.stext').val()
                }
            })
            // var QueryData = ReturnQuery();
        },
        deleteOne:function(){
            // console.log(data);
            // var duserId=ids.join(',');
            // console.log(b);
            var confirm1 = layer.confirm('确定删除所选信息？', {
                btn: ['确定', '取消'] //可以无限个按钮
            }, function(index, layero){
                //按钮【按钮一】的回调
                ids1.push(ids2);
                dele(ids1);
            }, function(index){
                //按钮【按钮二】的回调
                layer.close(confirm1);

            });

        },
        deleteMore:function(){
            // console.log(data);
            // var duserId=ids.join(',');
            // console.log(b);
            var confirm1 = layer.confirm('确定删除所选信息？', {
                btn: ['确定', '取消'] //可以无限个按钮
            }, function(index, layero){
                //按钮【按钮一】的回调
                // ids1.push(ids2);
                dele(ids);
            }, function(index){
                //按钮【按钮二】的回调
                layer.close(confirm1);

            });

        }

    }
    var active_veiw_2 = {
        ShowPage: function () {
            common.showModel(['view_2']);
        },
        //编辑函数
        editsave:function () {
            // alert(123);
            var email = $('.email').val();
            var updata = {
                userId:$('.usernumber').val(),
                userName:$('.username').val(),
                telephone:$('.phonenumber').val(),
                email:email,
                roleId:$('#select').val(),
            }
            console.log(JSON.stringify(updata));
            common.ajax_post('/HRSSPortal/user/updateAdmin', JSON.stringify(updata), true,
                function (data) {
                    console.log(data);
                    if(data.code==0){
                        var confirm = layer.confirm('修改成功', {
                            btn: ['确定'] //可以无限个按钮
                        }, function(index, layero){
                            //按钮的回调
                            layer.close(confirm);
                            active_veiw_1.ShowPage();
                        });

                    }
                })
        },
        //新建函数
        newsave:function(){
            // alert(123);
            var accountId = $('.usernumber').val();
            var userName = $('.username').val();
            var telephone = $('.phonenumber').val();
            var email = $('.email').val();
            var password = $('.date').val();
            var roleId = new Array();
            roleId.push($('#select').val());
            // $('label.tt').val("工号")
            var newdata={
                accountId:accountId,
                password:password,
                userName:userName,
                telephone:telephone,
                email:email,
                role:roleId
            }
            console.log(JSON.stringify(newdata));
            common.ajax_post('/HRSSPortal/user/create', JSON.stringify(newdata), true,
                function (data) {
                    console.log(data);
                    if(data.code==0){
                        // confirm('添加成功' ,'确定', function (index,layero) {
                        //     layer.close(confirm);
                        //     active_veiw_1.ShowPage();
                        // })
                        var confirm = layer.confirm('添加成功', {
                            btn: ['确定'] //可以无限个按钮
                        }, function(index, layero){
                            //按钮的回调
                            layer.close(confirm);
                            active_veiw_1.ShowPage();
                        });
                    }
                })
        },
    }
    page_Load();
    function page_Load() {
        active_veiw_1.ShowPage();
    }
    // 点击新建按钮
    $('.bigBtn a').on('click',function () {
        // alert(123);
        var othis = $(this), method = othis.data('method');
        if(method == "new"){
            $('.tt').html('工号');
            active_veiw_2.ShowPage();
            node.dataset.method = 'newsave';
            loadRootdept();
        } else if(method =="delete"){
            // if()
            active_veiw_1.deleteMore();
            // active_veiw_1[method] ? active_veiw_1[method].call(this, othis) : '';
        }

    })
    $('.layui-form .layui-btn-sm').on('click', function () {
        var othis = $(this), method = othis.data('method');
        active_veiw_1[method] ? active_veiw_1[method].call(this, othis) : '';
    });
    $('.btn button').on('click',function (obj) {
        var othis = $(this), method = othis.data('method');
        // console.log(method);
        if(method=="Query"){
            // active_veiw_1.ShowPage();
            common.showModel(['view_1']);
            active_veiw_1[method] ? active_veiw_1[method].call(this, othis) : '';
        }else if(method=="editsave"){
            active_veiw_2[method] ? active_veiw_2[method].call(this, othis) : ''
        }else if(method=="newsave"){
            active_veiw_2[method] ? active_veiw_2[method].call(this, othis) : ''
        }
    })
    function initview_1_table_1() {

        // alert(123);
        var id = $('.stext').val();
        var name = $('.sname').val();
        var index = layer.load(1, {
            shade: [0.1, '#fff'] //0.1透明度的白色背景
        });//转圈圈
        // var a = common.ajax_g_url();
        // console.log( common.ajax_g_url());
        view_1_table_1_ins = table.render({
            elem: '#view_1_table_1'
            ,url:common.ajax_g_url() + '/HRSSPortal/user/getData'
            ,where:{
                "search":'',
                "id":''
            }
            ,method:"post"
            , cellMinWidth: 100
            , cols: [[
                {type:'checkbox',title:'全选'}
                // ,{title: '',align: 'center',templet: function (d) {
                //         return '<img2 src="../../img2/u285.png" class="edit" lay-event="edit">'
                //     }}
//             ,{field:'id', width:80, title: 'ID', sort: true}
                ,{field:'userName',  title: '用户名', }
                ,{field:'userId',  title: '用户编号'}
                ,{field:'telephone',  title: '手机号码'}
                ,{field:'createTime', title: '入职日期'}
                // ,{field:'',  title: '部门'}
                // ,{field:'',  title: '职位'}
                // ,{field:'',  title: '角色'}
                ,{field:'email',  title: '邮箱'}
                ,{field:'enable',  title: '状态',templet: function (d) {
                        return  d.enable == '1' ? "在职":"离职";
                    } }
                ,{field:'updateBy',  title: '修改者',
                        // return d.role==null?"null":d.role.updateBy;
                        // console.log(d.role);
                 }
                ,{field:'updateTime',  title: '修改日期',
                        // return d.role==null?"null":d.role.updateTime;
                        // return  d.role.updateTime;
                }
                ,{field:'',  title: '操作',align:'center', templet:function(d){
                       return  '<a class="app-btn" lay-event="edit" > <i class="iconfont icon-bianji"></i>编辑</a>' +
                                '<a class="app-btn" lay-event="del" > <i class="iconfont icon-lajitong"></i>删除</a>'+
                                '<a class="app-btn" lay-event="export" > <i class="iconfont icon-daochu"></i>导出</a>';
                    }
            }
            ]]
            , done: function (res, curr, count) {
                console.log(res);
                table_data = res.data;
                if(res.code!=0){
                    $('.layui-none').html('暂无相关数据');
                }
                layer.close(index);
            }
        });
    }
    table.on('tool(view_1_table_1)', function (obj) {
        var data = obj.data;
        // console.log(data.role.roleId);
        if (obj.event === 'edit') {
            common.show_title('title', '编辑');
            console.log(data);
            $('.tt').html('用户编号');
            common.input_disabled(".usernumber");
            // $(".usernumber").attr('disabled','disabled');
            $('.usernumber').val(data.userId);
            $('.username').val(data.userName);
            $('.phonenumber').val(data.telephone);
            $('.email').val(data.email);
            $('.ruzhi').hide();
            active_veiw_2.ShowPage();
            morenId = data.roleId;
            loadRootdept();
            node.dataset.method = 'editsave';
            // console.log($("#select option:selected").val());
        }
        if(obj.event === 'del'){
            // console.log(data);
            ids2=obj.data.userId;
            active_veiw_1.deleteOne();
        }
    })
    // 获取复选框id
    table.on('checkbox(view_1_table_1)', function(obj){
        if(obj.checked==true){
            if(obj.type=="one"){
                ids.push(obj.data.userId);
                // console.log(ids);
            }else{
                for(var i=0;i<table_data.length;i++){
                    ids.push(table_data[i].userId);
                }
            }
        }
        // console.log(ids);
        // console.log(obj.checked); //当前是否选中状态
        // console.log(obj.data.userId); //选中行的相关数据
        // console.log(obj.type); //如果触发的是全选，则为：all，如果触发的是单选，则为：one
    });
    // 监听表单提交
    //监听提交
    form.on('submit', function(data){
        return false; //阻止表单跳转
    });
    function dele(data){
    common.ajax_post('/HRSSPortal/user/delete', {userId:data}, true,
        function (data) {
            console.log(data);
            if(data.code==0){
                var confirm = layer.confirm('删除成功', {
                    btn: ['确定'] //可以无限个按钮
                }, function(index, layero){
                    //按钮的回调
                    layer.close(confirm);
                    active_veiw_1.ShowPage();
                });
            }else{
                layer.close(confirm);
                layer.msg("删除失败", { icon: 5 });
            }
        })
    }
    // 从后台获取下拉框的数据
    function loadRootdept(){
        common.Select(morenId, { id: "roleId", text: "roleName" }, "/HRSSPortal/role/getData", "post", {search:"",id:""}, "select");
        // $('.layui-this').html()
    }
    function ReturnQuery() {
        return {
            "CCNO": $.trim($("#QCCNO").val()),
            // "CCNO2: $.trim($("#QCCNO").val())        }
            // console.log($("#QCCNO").val())
        }
    }
})



