var table, $, laydate, layer, common, element, rate, form;
var userinfo = JSON.parse(sessionStorage.getItem("userInfo"));
layui.use(['element', 'rate', 'table', 'layer', 'common', 'form', 'laydate', 'formSelects'], function () {
    table = layui.table
        , $ = layui.jquery
        , laydate = layui.laydate
        , layer = layui.layer
        , common = layui.common
        , element = layui.element
        , rate = layui.rate
        , formSelects = layui.formSelects
        , form = layui.form;
});

var app = new Vue({
    el: '#app',
    data: {
        ws: null,
        assigeuserlist: "",
        newtaskorder: {
            orderType: "",
            origin: "",
            taskTpId: 0,
            qty: 0,
            target: 0,
            taskId: null,
            toolId: null,
            assigeTo: "",
            remark: "",
            priority: 10,
            planEndTime: ""
        },
        orderdatalist: [],
        orderdetial: {
            createBy: "",
            orderNo: "",
            originName: "",
            targetName: "",
            taskTpDescr: "",
            orderToolList: [],
            priority: 10
        },
        noticedetial: null,
        layerindex: {
            index_1: 1
        },
        ws: null
    },
    created: function () {
        var that = this;
        setTimeout(function () {
            that.initpage();
        }, 1000);
    },
    methods: {
        initpage: function () {

            var that = this;
            that.orderassign();
            that.newoder();
            that.orderlist();
            that.shownotice();

            var a = common.ajax_g_url().substring(7, 26);
            if (WebSocket) {
                //建立连接
                that._data.ws = new WebSocket("ws://" +a+"/HRSSPortal/webSocket/" + userinfo.userId);
            } else {
                alert("浏览器不支持WebSocket！");
            }

            that._data.ws.onmessage = function (event) {
                console.log(event);
                var obj = JSON.parse(event.data);
                if (obj.msg === "order") {
                    parent.app.wsdata = obj.data;
                } else {
                    that._data.noticedetial = obj.data;
                }
            };

            form.on('checkbox(priority)', function (data) {
                if (this.checked) {
                    that._data.newtaskorder.priority = 20;
                } else {
                    that._data.newtaskorder.priority = 10;
                }
            });

            form.on('select(taskTpId)', function (data) {

                common.ajax_post('/HRSSPortal/task/getData', { "taskTpId": data.value }, true,
                    function (data) {
                        if (data.code === 0) {
                            var a = common.formSelects2("", { id: "taskId", text: "descr" }, data.data);
                            formSelects.data('taskId', 'local', a);
                        }
                    });
            });

            form.on('select(origin)', function (data) {
                common.Select("", { id: "userId", text: "userName" }, "/HRSSPortal/order/getShipper", "post", { "deptId": data.value }, "assigeTo");
            });
        },
        orderassign: function () {
            var that = this;
            common.ajax_post('/HRSSPortal/order/getShipper', null, true,
                function (data) {
                    console.log(data);
                    if (data.code === 0) {
                        that._data.assigeuserlist = data.data;
                    }
                });
        },
        newoder: function () {
            laydate.render({
                elem: '#planEndTime',
                value: common.nowDate().shortDate
            });
            common.Select("", { id: "deptId", text: "deptName" }, "/HRSSPortal/dept/getDept", "post", null, "origin");
            common.Select("", { id: "taskTpId", text: "descr" }, "/HRSSPortal/taskType/getData", "post", null, "taskTpId");
            common.Select("", { id: "deptId", text: "deptName" }, "/HRSSPortal/dept/getDept", "post", null, "target");
            common.Select("", { id: "userId", text: "userName" }, "/HRSSPortal/order/getShipper", "post", null, "assigeTo");
            common.ajax_post('/HRSSPortal/trantool/getDataTbtrantool', null, true,
                function (data) {
                    if (data.code === 0) {
                        var a = common.formSelects2("", { id: "tool_id", text: "descr" }, data.data);
                        formSelects.data('toolId', 'local', a);
                    }
                });
            common.ajax_post('/HRSSPortal/task/getData', null, true,
                function (data) {
                    if (data.code === 0) {
                        var a = common.formSelects2("", { id: "taskId", text: "descr" }, data.data);
                        formSelects.data('taskId', 'local', a);
                    }
                });
        },
        saveneworder: function () {
            var that = this;
            that._data.newtaskorder.planEndTime = $("#planEndTime").val() + " " + "00:00:00";
            that._data.newtaskorder.taskTpId = $("#taskTpId").val();
            that._data.newtaskorder.orderType = "OT01";
            that._data.newtaskorder.taskId = formSelects.value('taskId', 'val');
            that._data.newtaskorder.toolId = formSelects.value('toolId', 'val');
            that._data.newtaskorder.remark = $("#remark").val();
            that._data.newtaskorder.origin = $("#origin").val();
            that._data.newtaskorder.target = $("#target").val();
            that._data.newtaskorder.assigeTo = $("#assigeTo").val();

            if (that._data.newtaskorder.taskTpId === null || that._data.newtaskorder.taskId.length === 0 || that._data.newtaskorder.origin === null || that._data.newtaskorder.target === null || that._data.newtaskorder.target === "") {
                return;
            }

            var a = JSON.stringify(that._data.newtaskorder);
            common.ajax_post('/HRSSPortal/order/CreateDispatchOrder', JSON.stringify(that._data.newtaskorder), true,
                function (data) {
                    if (data.code === 0) {
                        that.cleanneworder();
                        layer.msg(data.msg, { icon: 1 });
                    } else {
                        layer.msg(data.msg, { icon: 5 });
                    }
                });
        },
        orderlist: function () {
            var that = this;
            common.ajax_post('/HRSSPortal/order/getOrderWeb', JSON.stringify({ "isAssige": 0 }), true,
                function (data) {
                    if (data.code === 0) {
                        var a = JSON.stringify(data.data[0]);
                        that._data.orderdatalist = data.data;
                    }
                });
        },
        showorderdetial: function (d) {
            var that = this;
            that._data.orderdetial = d;
            that._data.layerindex.index_1 = layer.open({
                type: 1,
                offset: 'auto',
                shade: 0.1,
                title: '<span><i class="iconfont icon-yonghuguanli" style="color:#3298cf"></i>&nbsp;任务详情</span>',
                content: $("#view_1"),
                area: ["500px", "350px"]
            });
        },
        shownotice: function () {
            var that = this;
            common.ajax_post('/HRSSPortal/notice/getData', null, true,
                function (data) {
                    if (data.code === 0) {
                        that._data.noticedetial = data.data;
                    }
                });
        },
        cleanneworder: function () {
            common.Select_render([], "taskTpId");
            formSelects.value('taskId', []);
            formSelects.value('toolId', []);
            $("#remark").val("");
            common.Select_render([], "priority");
            common.Select_render([], "origin");
            common.Select_render([], "target");
            common.Select_render([], "assigeTo");

        },
        assign: function (d) {
            common.Select_render([d.userId], "assigeTo");
        }
    }
});