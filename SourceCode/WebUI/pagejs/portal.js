﻿
var urlhome = "";
var table, $, laydate, layer, common, element, rate, form, formSelects;
var userinfo = JSON.parse(sessionStorage.getItem("userInfo"));
if (userinfo.role.roleId === 2) {
    document.getElementById("framea").src = "../GodentryOrder/GO001.html";
    urlhome = "panel.html";
} else if(userinfo.role.roleId === 5) {
    document.getElementById("framea").src = "../UserManage/ST001.html";
    urlhome = "../UserManage/ST001.html";
} else if (userinfo.role.roleId === 3) {
    document.getElementById("framea").src = "main.html";
    urlhome = "main.html";
}
layui.use(['element', 'rate', 'table', 'layer', 'common', 'form', 'laydate', 'formSelects'], function () {
    table = layui.table
        , $ = layui.jquery
        , laydate = layui.laydate
        , layer = layui.layer
        , common = layui.common
        , element = layui.element
        , rate = layui.rate
        , formSelects = layui.formSelects
        , form = layui.form;
});

var app = new Vue({
    el: '#app',
    data: {
        menuobj: {
            homeurl: "",
            menu:[]
        },
        layerindex: {
            index_1: 1,
            index_2: 2,
            index_3: 3,
            index_4: 4,
            index_5: 5
        },
        ws: null,
        wsdata: {
            count: 0,
            order: []
        },
        assigeuser: {
            assigeuserlits: "",
            order: ""
        }, orderdetial: {
            createBy: "",
            orderNo: "",
            originName: "",
            targetName: "",
            taskTpDescr: "",
            orderToolList: [],
            priority: 10
        },
        imgurl:"../../img2/admin.png",
        username:"",
    },
    created: function () {
        var that = this;
        setTimeout(function () {
            that.initpage();
        }, 1000);
    },
    updated: function(){
        form.render();
        element.init();
    },
    methods: {
        initpage: function () {
            var that = this;
            common.ajax_post('/HRSSPortal/user/getAuthPage', null, true,
                function (data) {
                    if (data.code === 0) {
                        console.log(data);
                        if (data.msg == "SUCC") {
                            that._data.menuobj.homeurl = urlhome;
                            that._data.menuobj.menu = data.data;
                        } else {
                            layer.msg(data.message, { icon: 5 });
                        }
                    }
                });

            that._data.username = JSON.parse(sessionStorage.getItem("userInfo")).userName;
            if (userinfo.headImg == null || userinfo.headImg == "") {
                that._data.imgurl="../../img2/admin.png";
            } else {
                that._data.imgurl = common.ajax_g_url()+"/HRSSPortal/commonFile/getImg/"+userinfo.headImg;
            }
            that.orderlist();
            form.render();
            element.init();

        },
        showtask: function () {
            var that = this;
            that._data.layerindex.index_1 = layer.open({
                type: 1,
                offset: 'auto',
                shade: 0.1,
                title: '<span><i class="iconfont icon-yonghuguanli" style="color:#3298cf"></i>&nbsp;任务调度</span>',
                content: $("#renwu"),
                area: ["750px", "400px"]
            });
            that.orderlist();
        },
        assign: function (d) {
            var that = this;
            that._data.orderdetial = d;
            that._data.layerindex.index_2 = layer.open({
                type: 1,
                offset: 'auto',
                shade: 0.1,
                title: '<span><i class="iconfont icon-yonghuguanli" style="color:#3298cf"></i>&nbsp;任务调度</span>',
                content: $("#diaodu"),
                area: ["600px", "500px"]
            });
            console.log(d);
            common.ajax_post('/HRSSPortal/order/getShipper', {'origin':d.origin}, true,
                function (data) {
                    console.log(data);
                    if (data.code === 0) {
                        that._data.assigeuser.orderNo = d.orderNo;
                        that._data.assigeuser.assigeuserlits = data.data;
                    }
                });
        },
        assignto: function (d) {
            var that = this;
            var confirm = layer.confirm('请确认派送！', {
                btn: ['确定', '取消'] //按钮
                , title: '提示',
            }, function () {
                common.ajax_post('/HRSSPortal/order/orderAssign', { "orderNo": that._data.assigeuser.orderNo, "userId": d.userId }, true,
                    function (data) {
                        if (data.code === 0) {
                            layer.msg("派遣成功!", { icon: 1 });
                            layer.close(that._data.layerindex.index_4);
                        } else {
                            layer.msg("派遣失败!", { icon: 5 });
                        }
                    });
            }, function () {
                layer.close();
            });
        },
        backstep: function () {
            var that = this;
            layer.close(that._data.layerindex.index_2);
        },
        ordercount: function () {
            common.ajax_post('/HRSSPortal/order/getNoAssigeOrder', null, true,
                function (data) {
                    if (data.code === 0) {
                        that._data.wsdata.count = data.data;
                    }
                });
        },
        orderlist: function () {
            var that = this;
            common.ajax_post('/HRSSPortal/order/getOrderWeb',{ "isAssige": 0 }, true,
                function (data) {
                    if (data.code === 0) {
                        var a = JSON.stringify(data.data);
                        that._data.wsdata.count = data.data.length;
                        that._data.wsdata.order = data.data;
                    }
                });
        },
        modifyuserinfo: function () {
            var that = this;
            that._data.layerindex.index_3 = layer.open({
                type: 1,
                offset: 'auto',
                shade: 0.1,
                title: '<span><i class="iconfont icon-yonghuguanli" style="color:#3298cf"></i>&nbsp;个人信息编辑</span>',
                content: $("#editview"),
                area: ["850px", "600px"]
            });
        },

    }
});